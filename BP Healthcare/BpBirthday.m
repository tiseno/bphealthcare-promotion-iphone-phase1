//
//  BpBirthday.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpBirthday.h"

@implementation BpBirthday
@synthesize resultCount;
@synthesize birthday_imgID;
@synthesize birthday_imgPath;

-(void)dealloc
{
    [birthday_imgPath release];
    [birthday_imgID release];
    [resultCount release];
    [super dealloc];
}


@end
