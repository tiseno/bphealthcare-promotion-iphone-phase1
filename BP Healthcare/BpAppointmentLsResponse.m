//
//  BpAppointmentLsResponse.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpAppointmentLsResponse.h"

@implementation BpAppointmentLsResponse
@synthesize AppointmentLsArr;

-(void)dealloc
{
    [AppointmentLsArr release];
    [super dealloc];
}

@end
