//
//  BpAppRemarkViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpAppRemarkViewController.h"

@interface BpAppRemarkViewController ()

@end

@implementation BpAppRemarkViewController
@synthesize btnrmkdone;
@synthesize txtRemark;
@synthesize gBpNewAppointmentViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpAppRemarkViewController *gBpAppRemarkViewController=[[BpAppRemarkViewController alloc] initWithNibName:@"BpAppRemarkViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];    
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(DoneRemarktapped:) forControlEvents:UIControlEventTouchUpInside];   
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: rightButton] autorelease]; 
    
    gBpAppRemarkViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    gBpAppRemarkViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    [rightButton release];
    [leftButton release];
    [gBpAppRemarkViewController release];
    
    [[txtRemark layer] setBorderWidth:1];
	[[txtRemark layer] setCornerRadius:10];
    txtRemark.layer.borderColor=[UIColor colorWithRed:(170/255.0) green:(170/255.0) blue:(170/255.0) alpha:1.0].CGColor;
    self.btnrmkdone.hidden=YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
    
    [txtRemark becomeFirstResponder];
}

- (void)viewDidUnload
{
    [self setTxtRemark:nil];
    [self setBtnrmkdone:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    heightOfEditedView = textView.frame.size.height;
    heightOffset = textView.frame.origin.y+160;
    
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    
}

-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    //iskeyboardDisplayed=NO;
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]){
        [txtRemark resignFirstResponder];
        return NO;
    }else{
        return YES;
    }
}

-(IBAction)DoneRemarktapped:(id)sender
{
    UILabel *lblServiceTypeSelected=(UILabel*)[self.gBpNewAppointmentViewController lblRemark];
    
    lblServiceTypeSelected.text=txtRemark.text;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc 
{
    [gBpNewAppointmentViewController release];
    [txtRemark release];
    [btnrmkdone release];
    [super dealloc];
}

@end
