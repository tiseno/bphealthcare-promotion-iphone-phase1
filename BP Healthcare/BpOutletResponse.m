//
//  BpOutletResponse.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpOutletResponse.h"

@implementation BpOutletResponse
@synthesize OutletArr;

-(void)dealloc
{
    [OutletArr release];
    [super dealloc];
}
@end
