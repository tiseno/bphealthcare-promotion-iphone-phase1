//
//  BpEventViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkHandler.h"
#import "BpEvent.h"
#import "BpEventRequest.h"
#import "BpEventResponse.h"
#import "Reachability.h"
#import "BpEventTableViewController.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "LoadingView.h"
#import "BpAppDelegate.h"
#import "MTPopupWindow.h"
#import "BpPromotionImageViewController.h"

@interface BpEventViewController : UIViewController<NetworkHandlerDelegate,BpEventDelegate,UITableViewDelegate>
{
    
}
-(void)getEvent;
@property (retain , nonatomic) BpEventTableViewController* gBpEventTableViewController;
//@property (nonatomic, retain) BpEventTableViewController* gBpEventTableViewController;
@property (retain, nonatomic) IBOutlet UITableView *tblEvent;
@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic, retain) EKEventViewController *detailViewController;
@property (nonatomic, retain) EKEventStore *eventStore;
@property (nonatomic, retain) EKCalendar *defaultCalendar;
@property (nonatomic, retain) NSMutableArray *eventsList;
@property (retain, nonatomic) IBOutlet UIImageView *Bgimg;
@property (retain, nonatomic) IBOutlet UILabel *lblnoItem;

@end
