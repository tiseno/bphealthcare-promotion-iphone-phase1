//
//  BpMainViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BptestingViewController.h"
#import "BpAppointmentViewController.h"
#import "BpBirthdayViewController.h"
#import "BpCheckScheduleViewController.h"
#import "BpYourPointViewController.h"
#import "BpOurOutletViewController.h"
#import "BpLatestPromotionViewController.h"
#import "BpCreditCardPromotionViewController.h"
#import "BpFestivalViewController.h"
#import "BpCouponViewController.h"
#import "BpEventViewController.h"
#import "BpHealthTipsViewController.h"
#import "Bplogin.h"
#import "BploginRequest.h"
#import "BploginResponse.h"
#import "NetworkHandler.h"
#import "Reachability.h"
#import "LoadingView.h"
#import "BpAppDelegate.h"
#import "BpOutletStateViewController.h"

@interface BpMainViewController : UIViewController<NetworkHandlerDelegate>{
    BOOL loginStatus;
    
    int counterA;
    bool startA;
    NSString *_CountDownTime;
    NSTimer *timerA;
    UIButton *rightButton;
}
@property (retain, nonatomic) UIButton *rightButton;
@property (retain, nonatomic) IBOutlet UIImageView *imgSplash;
@property (nonatomic, retain) LoadingView *loadingView;
@property(nonatomic,retain) UITextField *textfieldName;
@property(nonatomic,retain) UITextField *textfieldPassword;
@property (nonatomic, retain) UIButton *leftButton;
@property (nonatomic, retain) UIButton *leftloginButton;
@property (nonatomic, retain) UIButton *leftlogoutButton;
-(IBAction)AppointmentTapped;
-(IBAction)testingTapped;
@property (retain, nonatomic) IBOutlet UIScrollView *mainScrolView;

-(IBAction)BirthdayTapped;
-(IBAction)CheckScheduleTapped;
-(IBAction)YourPointTapped;
-(IBAction)OurOutletTapped;
-(IBAction)LatestPromotionTapped;
-(IBAction)CreditCardPromotionTapped;
-(IBAction)FestivalTapped;
-(IBAction)CouponTapped;
-(IBAction)EventTapped;
-(IBAction)HealthTipsTapped;
- (void) someMethod;
@property (retain, nonatomic) IBOutlet UIButton *btnbirthday;
@property (retain, nonatomic) IBOutlet UILabel *lblbirthday;
@property (retain, nonatomic) IBOutlet UIImageView *icaonbirthday;
@property (retain, nonatomic) IBOutlet UIImageView *arrowbirthday;

@end
