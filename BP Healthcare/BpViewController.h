//
//  BpViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpSplashViewController.h"
#import "BpMainViewController.h"

@interface BpViewController : UIViewController{
    int counterA;
    bool startA;
    NSString *_CountDownTime;
    NSTimer *timerA;
}

@property (nonatomic,retain) UINavigationController* navController;
@property (nonatomic,retain) BpMainViewController *gBpMainViewController;
@property (retain, nonatomic) IBOutlet UIImageView *imgslpashView;

@end
