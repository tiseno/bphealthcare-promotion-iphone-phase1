//
//  BpOutletStateViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpOutletStateViewController.h"

@interface BpOutletStateViewController ()

@end

@implementation BpOutletStateViewController
@synthesize tblOutletState;
@synthesize State, SelectedState;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc 
{
    [SelectedState release];
    [State release];
    [tblOutletState release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpOutletStateViewController *gBpOutletStateViewController=[[BpOutletStateViewController alloc] initWithNibName:@"BpOutletStateViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpOutletStateViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpOutletStateViewController release];
    
    tblOutletState.layer.cornerRadius=10;
    //tblOutletState.layer.borderColor = [UIColor grayColor].CGColor;
    self.tblOutletState.layer.borderColor = [UIColor colorWithRed:(170/255.0) green:(170/255.0) blue:(170/255.0) alpha:1.0].CGColor;
    tblOutletState.layer.borderWidth = 1;
    
    State = [[NSArray alloc] initWithObjects:@"Perlis", 
             @"Kedah", @"Penang", @"Kelantan", 
             @"Perak", @"Terengganu",
             @"Pahang", @"Selangor", 
             @"Kuala Lumpur", @"Negeri Sembilan",
             @"Melaka", @"Johor",
             @"Sarawak", @"Sabah",
             nil];
    [tblOutletState reloadData];
    
}

- (void)viewDidUnload
{
    [self setTblOutletState:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return State.count;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell.
    NSString * sushiName = [State objectAtIndex:indexPath.row];
    //NSString *sushiString = [[NSString alloc] initWithFormat:@"%d: %@", indexPath.row, sushiName];
    NSString *sushiString = [[NSString alloc] initWithFormat:@" %@", sushiName];
    cell.textLabel.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
    cell.textLabel.text = sushiString;
    [sushiString release];
    
    return cell; 
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString * sushiName = [State objectAtIndex:indexPath.row];
    NSString * sushiString = [NSString stringWithFormat:@"%@", sushiName];    
    NSString * message = [NSString stringWithFormat:@" %@", sushiString];
    /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Selected State" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
    [alertView release];*/
    
    
    //UILabel *lblServiceTypeSelected=(UILabel*)[self.gBpNewAppointmentViewController lblSelectedState];
    
    //lblServiceTypeSelected.text=sushiString;
    
    //[self.navigationController popViewControllerAnimated:YES];
    
    /*NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
     BpOutletRequest *loginset= [[BpOutletRequest alloc] initWithsState:sushiString];
     
     [networkHandler setDelegate:self];
     [networkHandler request:loginset];
     [loginset release];
     [networkHandler release];*/
    
    BpOurOutletViewController *gBpOurOutletViewController=[[BpOurOutletViewController alloc] initWithNibName:@"BpOurOutletViewController" bundle:nil];
    
    gBpOurOutletViewController.title=@"Our Outlets";
    gBpOurOutletViewController.SelectedState=sushiString;
    [self.navigationController pushViewController:gBpOurOutletViewController animated:YES];
    [gBpOurOutletViewController release];
    
    //_lastSushiSelected = sushiString;
    //SelectedState = [sushiString retain];
    
}


@end
