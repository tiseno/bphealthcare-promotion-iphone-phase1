//
//  XMLRequest.m
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/5/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "XMLRequest.h"


@implementation XMLRequest
@synthesize webserviceURL,SOAPAction,requestType,XMLNameSpace;
-(id)init
{
    self=[super init];
    if(self)
    {

        //self.webserviceURL=@"http://mobilesolutions.com.my/BP/bpAppWebServices.asmx";
        self.webserviceURL=@"http://203.106.245.234:100/bpAppWebServices.asmx";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(void)dealloc
{
    [webserviceURL release];
    [SOAPAction release];
    [XMLNameSpace release];
    [super dealloc];
}
-(NSString*) generateHTTPPostMessage
{
    [NSException raise:NSInternalInconsistencyException 
				format:@"Implementation is not provided", NSStringFromSelector(_cmd)];
	return nil;
}
-(NSString *)generateSOAPMessage
{
    [NSException raise:NSInternalInconsistencyException format:@"Implementation is not provided", NSStringFromSelector(_cmd)];
    
    return nil;
}
@end
