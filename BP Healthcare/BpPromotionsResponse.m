//
//  BpPromotionsResponse.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpPromotionsResponse.h"


@implementation BpPromotionsResponse
@synthesize PromotionsArr;

-(void)dealloc
{
    [PromotionsArr release];
    [super dealloc];
}
@end
