//
//  BpSplashViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpSplashViewController.h"

@interface BpSplashViewController ()

@end

@implementation BpSplashViewController
@synthesize gBpMainViewController, navController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpMainViewController *gtBpMainViewController=[[BpMainViewController alloc] initWithNibName:@"BpMainViewController" bundle:nil];
    
    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:gtBpMainViewController];
    
    tnavController.view.frame=CGRectMake(0, 0, 320, 416);
    gtBpMainViewController.title=@"BP Healthcare";
    UIImage *imagetopbar = [UIImage imageNamed:@"bar_top.png"];
    [tnavController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    [self.view addSubview:tnavController.view];
    //[self.navigationController pushViewController:gBpMainViewController animated:YES];
    //[self presentModalViewController:self.navController animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc
{
    [navController release];
    [gBpMainViewController release];
    [super dealloc];
}

@end
