//
//  BpAppointmentList.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpAppointmentList.h"

@implementation BpAppointmentList
@synthesize resultCount, adminRemark, appointmentID, appointmentSort, appointmentBranch, appointmentStatus, appointmentCreateDate, appointmentNoOfPerson, appointmentBookDateTime, appointmentServiceRequest, customerName, customerEmail, customerRemark, customerContact, insertResult, userID, tokenID;


-(void)dealloc
{
    [resultCount release];
    [appointmentID release];
    [appointmentServiceRequest release];
    [appointmentSort release];
    [appointmentStatus release];
    
    [appointmentNoOfPerson release];
    [appointmentCreateDate release];
    [appointmentBookDateTime release];
    [appointmentBranch release];
    [userID release];
    
    [tokenID release];
    [adminRemark release];
    [customerName release];
    [customerContact release];
    [customerEmail release];
    
    [customerRemark release];
    [insertResult release];
    
    [super dealloc];
}
@end





