//
//  BpAppPushCell.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppPushDelegate.h"
#import "BpAppointmentList.h"

@interface BpAppPushCell : UITableViewCell

@property (nonatomic, retain) id<AppPushDelegate> delegate;
@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic, retain) UILabel *lblTitle;
@property (nonatomic, retain) UILabel *lblBodytext;
@property (nonatomic, retain) UILabel *lblDate;
@property (nonatomic, retain) UILabel *lblDateText;
@property (nonatomic, retain) UILabel *lblRemark;
@property (nonatomic, retain) UILabel *lblRemarktext;
@property (nonatomic, retain) UILabel *lblStatus;
@property (nonatomic, retain) UILabel *lblStatustext;
@property (nonatomic, retain) UIImageView *imgsmallflag;
@property (nonatomic, retain) UIImageView *imgsmallcancel;
@property (nonatomic, retain) UIButton *btnsaveAppointment;
@property (nonatomic, retain) BpAppointmentList *AppointmentListcell;

@end
