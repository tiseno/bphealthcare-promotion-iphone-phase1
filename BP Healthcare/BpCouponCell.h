//
//  BpCouponCell.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpCoupon.h"
@interface BpCouponCell : UITableViewCell{
    
}

@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic,retain) UILabel *lblTitle;
@property (nonatomic, retain) UILabel *lblBodytext;
@property (nonatomic, retain) UILabel *lblBodyNoPictext;
@property (nonatomic, retain) UIImageView *imgsmallflag;
@property (nonatomic, retain) UIImageView *imgpicture;
@property (nonatomic, retain) BpCoupon *BpCouponcell;
@property (nonatomic, retain) NSString *url;

@end
