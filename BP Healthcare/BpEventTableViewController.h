//
//  BpEventTableViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpEvent.h"
#import "BpEventCell.h"
#import "BpAppDelegate.h"
#import "AsyncImageView.h"
#import "MTPopupWindow.h"
//#import "BpEventViewController.h"
@class BpEventViewController;
//@class BpPromotionImageViewController;
//#import "BpPromotionImageViewController.h"

@interface BpEventTableViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource>{
    BpEventCell *cell;
    UIView* bgView;
    UIView* bigPanelView;
}
//@property (nonatomic, retain) id<BpEventDelegate> delegate;
@property (nonatomic, retain) NSArray *eventArr;
-(void)initializeTableData;
@property (nonatomic, retain) BpEventViewController* gBpEventViewController;
@property (nonatomic, retain) NSString *enlargeImagePath;

@end
