//
//  BpAppointmentDateTimeViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UIDatePicker;
@class BpNewAppointmentViewController;

@interface BpAppointmentDateTimeViewController : UIViewController{
    
}

@property (retain, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, retain) BpNewAppointmentViewController *gBpNewAppointmentViewController;
@property (retain, nonatomic) IBOutlet UILabel *datelabel;
-(IBAction)selectedDatetapped:(id)sender;

@end
