//
//  BpHealthyTipsCell.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpHealthTips.h"
#import "AsyncImageView.h"
#import "MTPopupWindow.h"

@interface BpHealthyTipsCell : UITableViewCell{
    
}


@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic,retain) UILabel *lblTitle;
@property (nonatomic, retain) UILabel *lblBodytext;
@property (nonatomic, retain) UILabel *lblBodyNoPictext;
@property (nonatomic, retain) UIImageView *imgsmallflag;
@property (nonatomic, retain) UIImageView *imgpicture;
@property (nonatomic, retain) BpHealthTips *gBpHealthTips;

@property (nonatomic, retain) NSString *url;


@end
