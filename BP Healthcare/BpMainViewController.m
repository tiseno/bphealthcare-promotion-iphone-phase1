//
//  BpMainViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpMainViewController.h"

@interface BpMainViewController ()

@end

@implementation BpMainViewController
@synthesize imgSplash;
@synthesize mainScrolView, loadingView;
@synthesize textfieldName, textfieldPassword;
@synthesize leftButton, leftloginButton, leftlogoutButton;
@synthesize rightButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        loginStatus=NO;
    }
    return self;
}

- (void)updateTimer{ //Happens every time updateTimer is called. Should occur every second.
    
    counterA -= 1;
    _CountDownTime = [NSString stringWithFormat:@"%i", counterA];
    
    if (counterA < 0) // Once timer goes below 0, reset all variables.
    {
        [timerA invalidate];
        startA = TRUE;
        //counterA = 1;
        //self.imgSplash.hidden=YES;

        //[self mainViewPage];
    }
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    counterA = 1;
    startA = TRUE;
    
    if(startA == TRUE) //Check that another instance is not already running.
    {
        _CountDownTime = @"1";
        startA = FALSE;
        timerA = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
        
        
        
    }
    
    // Do any additional setup after loading the view from its nib.
    //BpMainViewController *gBpMainViewController=[[BpMainViewController alloc] initWithNibName:@"BpMainViewController" bundle:nil];
    //UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:gBpMainViewController];
    
    //gBpMainViewController.title=@"Main";
    
    //UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_back.png"] style:UIBarButtonItemStylePlain target:nil action:nil];  
    //self.navigationItem.backBarButtonItem = backButton;  
    //[backButton release];
    
    //UIImage *imagetopbar = [UIImage imageNamed:@"bar_top.png"];
    //[tnavController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    //[tnavController release];
    //[self.view addSubview:tnavController.view];
    
    //gBpMainViewController.navigationItem.leftBarButtonItem =nil; // [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    //[leftButton release];
    //[gBpMainViewController release];

    
    
    //self.imgSplash.hidden=YES;
    [self mainViewPage];
    
}

-(void)mainViewPage
{
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        
        NSLog(@"code for 4-inch screen");
        [self fView];
    } else {
        // code for 3.5-inch screen
        NSLog(@"code for 3.5-inch screen");
        [self tView];
    }
    
    
    BpMainViewController *gBpMainViewController=[[BpMainViewController alloc] initWithNibName:@"BpMainViewController" bundle:nil];
    /*UIButton **/leftloginButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftloginButton setImage:[UIImage imageNamed:@"btn_login.png"] forState:UIControlStateNormal];    
    leftloginButton.frame = CGRectMake(0, 0, 62, 33);
    [leftloginButton addTarget:self action:@selector(someMethod) forControlEvents:UIControlEventTouchUpInside]; 
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftloginButton] autorelease]; 
    gBpMainViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftloginButton];
    
    
    rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"icon_bday.png"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(-30, 0, 30, 30);
    [rightButton addTarget:self action:@selector(BirthdayTapped) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: rightButton] autorelease];
    
    gBpMainViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    //[rightButton release];

    
    [leftloginButton release];

    [gBpMainViewController release];
    
    self.mainScrolView.contentSize=CGSizeMake(self.mainScrolView.frame.size.width, 550);
    //self.mainScrolView.frame=CGRectMake(0, 0, 320, 300);
    self.mainScrolView.scrollEnabled=YES;
    self.btnbirthday.hidden=YES;
    self.lblbirthday.hidden=YES;
    self.icaonbirthday.hidden=YES;
    self.arrowbirthday.hidden=YES;
    rightButton.hidden=YES;
    
}

-(void)fView
{
    self.mainScrolView.frame=CGRectMake(0, 0, 320, self.mainScrolView.frame.size.height);
    //[self.view addSubview:self.scrollcontent];
    //self.btnconfirm.frame=CGRectMake(20, 450, 280, 47);
}
-(void)tView
{
    self.mainScrolView.frame=CGRectMake(0, 88, 320, 548);
    //[self.view addSubview:self.scrollcontent];
    //self.btnconfirm.frame=CGRectMake(20, 362, 280, 47);
    //self.scrollcontent.contentSize=CGSizeMake(self.scrollcontent.frame.size.width, 385);
}

-(void)btnlogout
{

    [leftloginButton setImage:[UIImage imageNamed:@"btn_login.png"] forState:UIControlStateNormal];
    loginStatus=NO;
    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate clearloginStatus];
    
    self.mainScrolView.contentSize=CGSizeMake(self.mainScrolView.frame.size.width, 550);
    //self.mainScrolView.frame=CGRectMake(0, 0, 320, 400);
    self.mainScrolView.scrollEnabled=YES;
    self.btnbirthday.hidden=YES;
    self.lblbirthday.hidden=YES;
    self.icaonbirthday.hidden=YES;
    self.arrowbirthday.hidden=YES;
    rightButton.hidden=YES;
    NSLog(@"logout!");
}

- (void)viewDidUnload
{
    [self setMainScrolView:nil];
    [self setImgSplash:nil];
    [self setBtnbirthday:nil];
    [self setLblbirthday:nil];
    [self setIcaonbirthday:nil];
    [self setArrowbirthday:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return interfaceOrientation == UIInterfaceOrientationPortrait;
}


- (void) someMethod
{
    if (!loginStatus) {
        /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please provide your IC/Passport" message:@""
                                                       delegate:self cancelButtonTitle:@"Cancel"  otherButtonTitles:@"Submit", nil];
        
        [alert addTextFieldWithValue:@"" label:@"IC/Passport"];
        //[alert addTextFieldWithValue:@"" label:@"Password"];
        
        // Username
        textfieldName = [alert textFieldAtIndex:0];
        
        textfieldName.clearButtonMode = UITextFieldViewModeWhileEditing;
        textfieldName.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        
        textfieldName.keyboardType = UIKeyboardTypeAlphabet;
        textfieldName.keyboardAppearance = UIKeyboardAppearanceAlert;
        textfieldName.autocorrectionType = UITextAutocorrectionTypeNo;
        
        // Password
        textfieldPassword = [alert textFieldAtIndex:1];
        textfieldPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
        textfieldPassword.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        textfieldPassword.keyboardAppearance = UIKeyboardAppearanceAlert;
        textfieldPassword.autocorrectionType = UITextAutocorrectionTypeNo;
        textfieldPassword.secureTextEntry = YES;
        
        [alert show];*/
        
        UIAlertView * myAlert = [[UIAlertView alloc] initWithTitle:@"Please provide your IC/Passport!" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit",nil];
        myAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
        //UITextField * alertTextField = [alert textFieldAtIndex:0];
        textfieldName= [myAlert textFieldAtIndex:0];
        //textfieldName.keyboardType = UIKeyboardTypeNumberPad;
        textfieldName.placeholder = @"IC/Passport";
        textfieldName.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        [myAlert show];
        [myAlert release];
        
    }else {
        [self btnlogout];
    }
    
}

-(BOOL)NetworkStatus
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus==NotReachable);
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Clicked the Submit button
    if (buttonIndex != [alertView cancelButtonIndex])
    {
        /**/if(![self NetworkStatus])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        else
        {
            NSLog(@"Name: %@", textfieldName.text);
            //NSLog(@"Password : %@", textfieldPassword.text);
            //[leftloginButton setImage:[UIImage imageNamed:@"btn_logout.png"] forState:UIControlStateNormal]; 
        
            //loginStatus=YES;

        if ([textfieldName.text length] !=0) {
            NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
            
            BploginRequest *loginset= [[BploginRequest alloc] initWithisicno:textfieldName.text];
            
            [networkHandler setDelegate:self];
            [networkHandler request:loginset];
            [loginset release];
            [networkHandler release];
            
            UIView *selfView=self.view;
            LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
            self.loadingView=temploadingView;

        }else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please provide your IC/Passport." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert autorelease];
            [alert show];
        }
                    
            /*
            BOOL isLoadingDisplayed=NO;
            for(UIView *subView in self.view.subviews)
            {
                if ([subView class]==[LoadingView class]) {
                    isLoadingDisplayed=YES;
                }
            }
            if(!isLoadingDisplayed)
            {
                UIView *selfView=self.view;
                LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
                self.loadingView=temploadingView;
            }*/
        }
    }
} 

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BploginResponse class]])
    {
        
        //NSArray *strMessage=[NSString stringWithFormat:@"%@",((BploginResponse*)responseMessage).loginArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BploginResponse*)responseMessage).loginArr;
    
        for (Bplogin *item in msgArr) 
        {
            //birthitem.resultCount;
            /*NSLog(@"birthitem.resultCount--->%@",item.resultCount);
            NSLog(@"birthitem.LoginStatus--->%@",item.LoginStatus);
            NSLog(@"birthitem.ICNO--->%@",item.ICNO);*/


            if (item.LoginStatus.length !=0) 
            {
                
                if (![item.LoginStatus isEqualToString:@"fail"]) {
                    [leftloginButton setImage:[UIImage imageNamed:@"btn_logout.png"] forState:UIControlStateNormal]; 
                    
                    loginStatus=YES;

                    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
                    appDelegate.loginStatus=item;
                    
                    //NSLog(@"appDelegate.ICNO--->%@",appDelegate.loginStatus.ICNO);
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Successful." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert autorelease];
                    [alert show];
                    
                    [self showbirthdayTapped];
                    
                }
                else 
                {
                    //NSLog(@"fail!!");
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login failed, please try again. Thank You." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert autorelease];
                    [alert show];
                }
            }else 
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login failed, please try again. Thank You." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert autorelease];
                [alert show];
            }
            [self.loadingView removeView];
        }
        
    }
}

-(void)showbirthdayTapped
{
    NSDate* currentDate = [NSDate date];
    
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc]init] autorelease];
    [dateFormat setDateFormat:@"ddMMyy"];
    NSString *dateString = [dateFormat stringFromDate:currentDate];
    
    NSString *strcurrentDate=[[[NSString alloc]initWithFormat:@"%@",dateString] autorelease];
    
    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSString *usericNo=[[[NSString alloc]initWithFormat:@"%@",appDelegate.loginStatus.ICNO] autorelease];
    usericNo= [usericNo substringFromIndex:2];
    usericNo= [usericNo substringToIndex:2];
    
    strcurrentDate= [strcurrentDate substringFromIndex:2];
    strcurrentDate= [strcurrentDate substringToIndex:2];
    
    NSLog(@"usericNo>>>>>%@",usericNo);
    
    if ([usericNo isEqualToString:strcurrentDate]) {
        self.mainScrolView.contentSize=CGSizeMake(self.mainScrolView.frame.size.width, 600);
        self.mainScrolView.scrollEnabled=YES;
        self.btnbirthday.hidden=NO;
        self.lblbirthday.hidden=NO;
        self.icaonbirthday.hidden=NO;
        self.arrowbirthday.hidden=NO;
        rightButton.hidden=NO;
    }else {
        self.mainScrolView.contentSize=CGSizeMake(self.mainScrolView.frame.size.width, 550);
        self.mainScrolView.scrollEnabled=YES;
        self.btnbirthday.hidden=YES;
        self.lblbirthday.hidden=YES;
        self.icaonbirthday.hidden=YES;
        self.arrowbirthday.hidden=YES;
        rightButton.hidden=YES;
        
    }

    
    
}


-(IBAction)testingTapped
{
    BptestingViewController *gBpMainViewController=[[BptestingViewController alloc] initWithNibName:@"BptestingViewController" bundle:nil];
    

    /*UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    //[leftButton setUserInteractionEnabled:NO];
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 26);
    [leftButton addTarget:self action:@selector(handleBack) forControlEvents:UIControlEventTouchUpInside];   
    //self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpMainViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"btn_back.png"] style:UIBarButtonItemStyleBordered target:self action:nil ];  
    self.navigationItem.backBarButtonItem = backButton;  
    [backButton release];*/
    
    gBpMainViewController.title=@"testing";
    [self.navigationController pushViewController:gBpMainViewController animated:YES];
    [gBpMainViewController release];
}


- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc 
{
    [rightButton release];
    [loadingView release];
    [leftlogoutButton release];
    [leftloginButton release];
    [leftButton release];
    [textfieldName release];
    [textfieldPassword release];
    [mainScrolView release];
    [imgSplash release];
    [_btnbirthday release];
    [_lblbirthday release];
    [_icaonbirthday release];
    [_arrowbirthday release];
    [super dealloc];
}

-(IBAction)AppointmentTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        BpAppointmentViewController *gBpAppointmentViewController=[[BpAppointmentViewController alloc] initWithNibName:@"BpAppointmentViewController" bundle:nil];
    
        gBpAppointmentViewController.title=@"Appointment";
        [self.navigationController pushViewController:gBpAppointmentViewController animated:YES];
        [gBpAppointmentViewController release];
    }
    

}

-(IBAction)BirthdayTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        BpBirthdayViewController *gBpBirthdayViewController=[[BpBirthdayViewController alloc] initWithNibName:@"BpBirthdayViewController" bundle:nil];
        gBpBirthdayViewController.title=@"Happy Birthday";
        [self.navigationController pushViewController:gBpBirthdayViewController animated:YES];
        [gBpBirthdayViewController release];
    }
}

-(IBAction)CheckScheduleTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        BpCheckScheduleViewController *gBpMainViewController=[[BpCheckScheduleViewController alloc] initWithNibName:@"BpCheckScheduleViewController" bundle:nil];
        
        gBpMainViewController.title=@"Check Up Schedule";
        [self.navigationController pushViewController:gBpMainViewController animated:YES];
        [gBpMainViewController release];
    }
    
}

-(IBAction)YourPointTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        BpYourPointViewController *gBpYourPointViewController=[[BpYourPointViewController alloc] initWithNibName:@"BpYourPointViewController" bundle:nil];
        
        gBpYourPointViewController.title=@"My Points";
        [self.navigationController pushViewController:gBpYourPointViewController animated:YES];
        [gBpYourPointViewController release];
    }
    
}

-(IBAction)OurOutletTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        //BpOutletStateViewController
        /*BpOurOutletViewController *gBpOurOutletViewController=[[BpOurOutletViewController alloc] initWithNibName:@"BpOurOutletViewController" bundle:nil];
        
        gBpOurOutletViewController.title=@"Our Outlets";
        [self.navigationController pushViewController:gBpOurOutletViewController animated:YES];
        [gBpOurOutletViewController release];*/
        
        BpOutletStateViewController *gBpOutletStateViewController=[[BpOutletStateViewController alloc] initWithNibName:@"BpOutletStateViewController" bundle:nil];
        
        gBpOutletStateViewController.title=@"Our Outlets";
        [self.navigationController pushViewController:gBpOutletStateViewController animated:YES];
        [gBpOutletStateViewController release];
    }
    
}

-(IBAction)LatestPromotionTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        BpLatestPromotionViewController *gBpLatestPromotionViewController=[[BpLatestPromotionViewController alloc] initWithNibName:@"BpLatestPromotionViewController" bundle:nil];
        
        gBpLatestPromotionViewController.title=@"Promotions";
        [self.navigationController pushViewController:gBpLatestPromotionViewController animated:YES];
        [gBpLatestPromotionViewController release];
    }
    
}

-(IBAction)CreditCardPromotionTapped
{
    BpCreditCardPromotionViewController *gBpCreditCardPromotionViewController=[[BpCreditCardPromotionViewController alloc] initWithNibName:@"BpCreditCardPromotionViewController" bundle:nil];
    
    gBpCreditCardPromotionViewController.title=@"Credit Card";
    [self.navigationController pushViewController:gBpCreditCardPromotionViewController animated:YES];
    [gBpCreditCardPromotionViewController release];
}

-(IBAction)FestivalTapped
{
    BpFestivalViewController *gBpFestivalViewController=[[BpFestivalViewController alloc] initWithNibName:@"BpFestivalViewController" bundle:nil];
    
    gBpFestivalViewController.title=@"Festival";
    [self.navigationController pushViewController:gBpFestivalViewController animated:YES];
    [gBpFestivalViewController release];
}

-(IBAction)CouponTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        BpCouponViewController *gBpCouponViewController=[[BpCouponViewController alloc] initWithNibName:@"BpCouponViewController" bundle:nil];
        
        gBpCouponViewController.title=@"Coupons";
        [self.navigationController pushViewController:gBpCouponViewController animated:YES];
        [gBpCouponViewController release];
    }
    
}

-(IBAction)EventTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        BpEventViewController *gBpEventViewController=[[BpEventViewController alloc] initWithNibName:@"BpEventViewController" bundle:nil];
        
        gBpEventViewController.title=@"Events";
        [self.navigationController pushViewController:gBpEventViewController animated:YES];
        [gBpEventViewController release];
    }
    
}

-(IBAction)HealthTipsTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        BpHealthTipsViewController *gBpHealthTipsViewController=[[BpHealthTipsViewController alloc] initWithNibName:@"BpHealthTipsViewController" bundle:nil];
        
        gBpHealthTipsViewController.title=@"Health Tips";
        [self.navigationController pushViewController:gBpHealthTipsViewController animated:YES];
        [gBpHealthTipsViewController release];
    }
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    if([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
    {
        //order information will be removed here
        //order information will be removed here
        //order information will be removed here
        //order information will be removed here
        NSLog(@"order information will be removed here11");
        
    }
    //else {
    //    NSLog(@"order information will be removed here22");
    //}
    //[self.navigationController viewDidDisappear:animated];
    [super viewDidDisappear:animated];
}


@end







