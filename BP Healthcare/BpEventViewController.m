//
//  BpEventViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpEventViewController.h"


@implementation BpEventViewController
@synthesize gBpEventTableViewController;
@synthesize tblEvent, loadingView;
@synthesize defaultCalendar, detailViewController, eventsList, eventStore;
@synthesize Bgimg, lblnoItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpEventViewController *gBpEventViewController=[[BpEventViewController alloc] initWithNibName:@"BpEventViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpEventViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpEventViewController release];
    
    [self getEvent];
    lblnoItem.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
    //self.tblEvent.rowHeight = 130;
    
    /*===========Event table================*/
    if(gBpEventTableViewController == nil)
    {
        BpEventTableViewController *gBpEventTableViewController = [[BpEventTableViewController alloc] init];
        self.gBpEventTableViewController = gBpEventTableViewController;
        self.gBpEventTableViewController.gBpEventViewController=self;
        [gBpEventTableViewController release];
    } 
    
    
    [tblEvent setDataSource:self.gBpEventTableViewController];
    [tblEvent setDelegate:self.gBpEventTableViewController];
    
    
    self.gBpEventTableViewController.view = self.gBpEventTableViewController.tableView;
    //self.gBpEventTableViewController.view = self.gBpEventTableViewController.view;
    
    /*===========Event calender================*/
    self.eventStore = [[EKEventStore alloc] init];
    self.eventsList = [[NSMutableArray alloc] initWithArray:0];
    // Get the default calendar from store.
    self.defaultCalendar = [self.eventStore defaultCalendarForNewEvents];
    
    // create an Add button
    UIBarButtonItem *addButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bell.png" ] style:UIBarButtonItemStylePlain target:self action:@selector(addEvent:)];
    //UIBarButtonItem *addButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(addEvent:)];
    self.navigationItem.rightBarButtonItem = addButtonItem;
    [addButtonItem release];

    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
}

- (void)viewDidUnload
{
    [self setTblEvent:nil];
    [self setBgimg:nil];
    [self setLblnoItem:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getEvent
{
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    BpEventRequest *requestset= [[BpEventRequest alloc] init];
    
    [networkHandler setDelegate:self];
    [networkHandler request:requestset];
    [requestset release];
    [networkHandler release];
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpEventResponse class]])
    {
        
        //NSString *strMessage=[NSString stringWithFormat:@"%@",((BpCheckUpResponse*)responseMessage).CheckUpArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpEventResponse*)responseMessage).EventArr;
        
        for (BpEvent *item in msgArr) {
            //birthitem.resultCount;
            /*
            NSLog(@"birthitem.resultCount--->%@",item.resultCount);
            NSLog(@"birthitem.eventsImgPath--->%@",item.eventsImgPath);
            NSLog(@"birthitem.eventsSort--->%@",item.eventsSort);
            
            NSLog(@"birthitem.eventsStatus--->%@",item.eventsStatus);
            NSLog(@"birthitem.eventsTitle--->%@",item.eventsTitle);
            NSLog(@"birthitem.eventsID--->%@",item.eventsID);
            
            NSLog(@"birthitem.eventsDescp--->%@",item.eventsDescp);
            NSLog(@"birthitem.eventsDateTime--->%@",item.eventsDateTime);
            NSLog(@"birthitem.eventsCreateDate--->%@",item.eventsCreateDate);
            */
            
            if (![item.resultCount isEqualToString:@"0"])
            {
                
                Bgimg.hidden=YES;
                lblnoItem.hidden=YES;
                
            }else
            {
                Bgimg.hidden=NO;
                lblnoItem.hidden=NO;
            }
        }
        
        BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        appDelegate.EventArr=msgArr;
        
        [self.gBpEventTableViewController initializeTableData];
        [tblEvent reloadData];
        [self.loadingView removeView];
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    } 
    
}
 
#pragma mark -

#pragma mark Add a new Event
-(IBAction) addEvent:(id)sender {
    
    self.eventStore = [[EKEventStore alloc] init];
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    //  event.title =@"Whatever you want your title to be";
    event.title = @"hello There~";// self.currentTitle;
    
    /**/NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
    NSDate *myDate = [df dateFromString: @"16/08/2012 02:00  pm"];
    //startDate=@"";
    
    event.startDate=myDate;
    event.endDate=[df dateFromString: @"16/08/2012 02:00  pm"];
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    event.allDay = YES;
    
    EKEventEditViewController *addController = [[EKEventEditViewController alloc] initWithNibName:nil bundle:nil];
    addController.event = event;
    addController.eventStore = self.eventStore;
    [self presentModalViewController:addController animated:YES];
    
    addController.editViewDelegate = self;
    [addController release];
    
}

-(void)enlargeEventimgTapped;
{
    
}
-(void)imgurlTapped:(NSString*)imagename
{
    //NSLog(@"hello image imagename~%@",imagename);
    
    NSString *itemDescription=[imagename stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    if (![itemDescription isEqualToString:@"none"]) {
        NSString *imagenamepath=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/event/a%@",itemDescription];
    
    BpPromotionImageViewController *gBpPromotionImageViewController=[[BpPromotionImageViewController alloc] initWithNibName:@"BpPromotionImageViewController" bundle:nil];
    gBpPromotionImageViewController.title=@"Events";
    gBpPromotionImageViewController.imgPath=imagenamepath;
    [self.navigationController pushViewController:gBpPromotionImageViewController animated:YES];
    [gBpPromotionImageViewController release];
    }
}

-(void)addEventTapped:(BpEvent*)iBpEvent
{
    //NSLog(@"iBpEvent.eventsDateTime-->%@",iBpEvent.eventsDateTime);
    
    self.eventStore = [[EKEventStore alloc] init];
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if (6<=[currSysVer intValue])
    {
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            /* This code will run when uses has made his/her choice */
        }];
    }else
    {
        NSLog(@"currSysVer>>> 5");
        
    }
    
    
    
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    //  event.title =@"Whatever you want your title to be";
    event.title = iBpEvent.eventsTitle;// self.currentTitle;
    event.notes = iBpEvent.eventsDescp;
    /**/NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yyyy hh:mm a"];
    NSDate *myDate = [df dateFromString: iBpEvent.eventsDateTime];

    
    //event.startDate=myDate;
    //event.startDate=[[NSDate alloc] initWithTimeInterval:0 sinceDate:myDate ];
    //event.endDate=[df dateFromString: iBpEvent.eventsDateTime];
    //event.endDate=[[NSDate alloc] initWithTimeInterval:0 sinceDate:myDate ];
    
    event.startDate = [[NSDate alloc] initWithTimeInterval:0 sinceDate:myDate]; //  works
    event.endDate = [[NSDate alloc] initWithTimeInterval:3600*1 sinceDate:event.startDate]; // works

    
    
    
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    event.allDay = NO;
    
    EKEventEditViewController *addController = [[EKEventEditViewController alloc] initWithNibName:nil bundle:nil];
    addController.event = event;
    addController.eventStore = self.eventStore;
    [self presentModalViewController:addController animated:YES];
    
    addController.editViewDelegate = self;
    [addController release];
}


#pragma mark -
#pragma mark EKEventEditViewDelegate

// Overriding EKEventEditViewDelegate method to update event store according to user actions.
- (void)eventEditViewController:(EKEventEditViewController *)controller 
          didCompleteWithAction:(EKEventEditViewAction)action {
    
    NSError *error = nil;
    EKEvent *thisEvent = controller.event;
    
    switch (action) {
        case EKEventEditViewActionCanceled:
            // Edit action canceled, do nothing. 
            break;
            
        case EKEventEditViewActionSaved:
            // When user hit "Done" button, save the newly created event to the event store, 
            // and reload table view.
            // If the new event is being added to the default calendar, then update its 
            // eventsList.
            if (self.defaultCalendar ==  thisEvent.calendar) {
                [self.eventsList addObject:thisEvent];
            }
            [controller.eventStore saveEvent:controller.event span:EKSpanThisEvent error:&error];
            //  [self.tableView reloadData];
            break;
            
        case EKEventEditViewActionDeleted:
            // When deleting an event, remove the event from the event store, 
            // and reload table view.
            // If deleting an event from the currenly default calendar, then update its 
            // eventsList.
            if (self.defaultCalendar ==  thisEvent.calendar) {
                [self.eventsList removeObject:thisEvent];
            }
            [controller.eventStore removeEvent:thisEvent span:EKSpanThisEvent error:&error];
            //[self.tableView reloadData];
            break;
            
        default:
            break;
    }
    // Dismiss the modal view controller
    [controller dismissModalViewControllerAnimated:YES];
    
}


// Set the calendar edited by EKEventEditViewController to our chosen calendar - the default calendar.
- (EKCalendar *)eventEditViewControllerDefaultCalendarForNewEvents:(EKEventEditViewController *)controller {
    EKCalendar *calendarForEdit = self.defaultCalendar;
    return calendarForEdit;
}

-(void)dealloc
{
    [loadingView release];
    [gBpEventTableViewController release]; 
    [tblEvent release];
    [Bgimg release];
    [lblnoItem release];
    [super dealloc];
}
@end
