//
//  BpLocation.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpLocation.h"

@implementation BpLocation
@synthesize latitude,longitude,altitude;
-(id)init
{
	self=[super init];
	if (self) {
		self.latitude=0.0;
		self.longitude=0.0;
		self.altitude=0.0;
	}
	return self;
}
-(id)initWithLatitude:(double)ilatitude longitude:(double)ilongitude
{
	self=[super init];
	if (self) {
		self.latitude=ilatitude;
		self.longitude=ilongitude;
		self.altitude=0;
	}
	return self;
}
- (double)distanceFromLocation:(const BpLocation *)location
{
	CLLocation *newLocation=[[CLLocation alloc] initWithLatitude:location.latitude
                                                       longitude:location.longitude];
	CLLocation *myLocation=[[CLLocation alloc] initWithLatitude:self.latitude
                                                      longitude:self.longitude];
	double distance=[myLocation distanceFromLocation:newLocation];
	[newLocation release];
	[myLocation release];
	return distance;
}
@end
