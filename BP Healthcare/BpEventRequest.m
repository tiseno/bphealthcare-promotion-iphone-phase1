//
//  BpEventRequest.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpEventRequest.h"

@implementation BpEventRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"retrieveEventsList";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}

@end
