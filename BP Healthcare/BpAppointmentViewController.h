//
//  BpAppointmentViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpAppointmentList.h"
#import "BpAppointmentLsRequest.h"
#import "BpAppointmentLsResponse.h"
#import "NetworkHandler.h"
#import "Reachability.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "BpAppointmentLsViewController.h"
#import "BpNewAppointmentViewController.h"
#import "LoadingView.h"
#import "BpAppDelegate.h"


@interface BpAppointmentViewController : UIViewController<NetworkHandlerDelegate>
{
    
}
-(void)NewAppointmentTapped;
-(void)getAppointmentList;
@property (nonatomic, retain) NSArray *AppointmentLsArr;
@property (retain, nonatomic) IBOutlet UITableView *tblAppointmentList;
@property (retain , nonatomic) BpAppointmentLsViewController* gBpAppointmentLsViewController;

@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic, retain) EKEventViewController *detailViewController;
@property (nonatomic, retain) EKEventStore *eventStore;
@property (nonatomic, retain) EKCalendar *defaultCalendar;
@property (nonatomic, retain) NSMutableArray *eventsList;
@property (retain, nonatomic) IBOutlet UIButton *btnaddNewAppoinment;
@property (retain, nonatomic) IBOutlet UIImageView *imgNewAppointment;
-(IBAction)gNewAppointmentTapped:(id)sender;
@end
