//
//  BpMyPoint.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpMyPoint.h"

@implementation BpMyPoint
@synthesize resultCount, varUserID, PointEarn, PointRebate, PointBalance, YrMth;

-(void)dealloc
{
    [YrMth release];
    [PointBalance release];
    [PointRebate release];
    [PointEarn release];
    
    [varUserID release];
    [resultCount release];
    [super dealloc];
}
@end


/*<resultCount>string</resultCount>
 <varUserID>string</varUserID>
 <PointEarn>string</PointEarn>
 <PointRebate>string</PointRebate>
 <PointBalance>string</PointBalance>
 <YrMth>string</YrMth>*/