//
//  LoadingView.h
//  Tupper
//
//  Created by Tiseno Mac 2 on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView
{

}

+ (id)loadingViewInView:(UIView *)aSuperview;
- (void)removeView;

@end
