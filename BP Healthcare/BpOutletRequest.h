//
//  BpOutletRequest.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"

@interface BpOutletRequest : XMLRequest{
    
}

@property (retain, nonatomic) NSString *state;
-(id)initWithsState:(NSString *)istate;
@end
