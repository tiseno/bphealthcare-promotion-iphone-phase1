//
//  BpEventResponse.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLResponse.h"

@interface BpEventResponse : XMLResponse
{
    
}
@property (nonatomic, retain) NSArray *EventArr;

@end
