//
//  BpMyPoint.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BpMyPoint : NSObject{
    
}

@property (nonatomic,retain) NSString *resultCount;
@property (nonatomic,retain) NSString *varUserID;
@property (nonatomic,retain) NSString *PointEarn;
@property (nonatomic,retain) NSString *PointRebate;

@property (nonatomic,retain) NSString *PointBalance;
@property (nonatomic,retain) NSString *YrMth;


@end


