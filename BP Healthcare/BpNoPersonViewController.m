//
//  BpNoPersonViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpNoPersonViewController.h"

@interface BpNoPersonViewController ()

@end

@implementation BpNoPersonViewController
@synthesize dropdown;
@synthesize lblNoofperson;
@synthesize txtNoPerson;
@synthesize gBpNewAppointmentViewController;
@synthesize Noperson;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpNoPersonViewController *gBpNoPersonViewController=[[BpNoPersonViewController alloc] initWithNibName:@"BpNoPersonViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpNoPersonViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];    
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(DoneNoPersontapped:) forControlEvents:UIControlEventTouchUpInside];   
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: rightButton] autorelease]; 
    
    gBpNoPersonViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton release];
    
    [leftButton release];
    [gBpNoPersonViewController release];
    
    [[txtNoPerson layer] setBorderWidth:1];
	[[txtNoPerson layer] setCornerRadius:15];
    
    person = [[NSMutableArray alloc]init];
    [person addObject:@"1"];
    [person addObject:@"2"];
    [person addObject:@"3"];
    [person addObject:@"4"];
    [person addObject:@"5"];
    [person addObject:@"6"];
    [person addObject:@"7"];
    [person addObject:@"8"];
    [person addObject:@"9"];
    [person addObject:@"10"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
    
}

- (void)viewDidUnload
{
    [self setTxtNoPerson:nil];
    [self setDropdown:nil];
    [self setLblNoofperson:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    heightOfEditedView = textView.frame.size.height;
    heightOffset = textView.frame.origin.y+160;
    
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    
}

-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    //iskeyboardDisplayed=NO;
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]){
        [txtNoPerson resignFirstResponder];
        return NO;
    }else{
        return YES;
    }
}

-(IBAction)DoneNoPersontapped:(id)sender
{
    if (Noperson.length !=0) {
        UILabel *lblServiceTypeSelected=(UILabel*)[self.gBpNewAppointmentViewController lblNoPerson];
        
        lblServiceTypeSelected.text=Noperson;
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)dealloc 
{
    [Noperson release];
    [gBpNewAppointmentViewController release];
    [txtNoPerson release];
    [dropdown release];
    [lblNoofperson release];
    [super dealloc];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView*)dropdown{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *) dropdown numberOfRowsInComponent:(NSInteger)component{
    return [person count];
}

-(NSString *)pickerView:(UIPickerView *)dropdown titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    
    return [person objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)dropdown didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    Noperson=[person objectAtIndex:row];
    self.lblNoofperson.text=Noperson;
}

@end
