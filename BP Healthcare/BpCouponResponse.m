//
//  BpCouponResponse.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpCouponResponse.h"

@implementation BpCouponResponse
@synthesize CouponArr;

-(void)dealloc
{
    [CouponArr release];
    [super dealloc];
}

@end
