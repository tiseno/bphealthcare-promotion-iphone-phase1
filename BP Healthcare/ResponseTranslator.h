//
//  ResponseTranslator.h
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/5/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IResponseTranslator.h"
#import "GDataXMLNode.h"
#import "BpBirthday.h"
#import "BpBirthdayResponse.h"
#import "BpCheckUp.h"
#import "BpCheckUpResponse.h"
#import "BpFestival.h"
#import "BpFestivalResponse.h"
#import "BpCoupon.h"
#import "BpCouponResponse.h"
#import "BpEvent.h"
#import "BpEventResponse.h"
#import "BpHealthTips.h"
#import "BpHealthTipsResponse.h"
#import "BpPromotions.h"
#import "BpPromotionsResponse.h"
#import "BpOutlet.h"
#import "BpOutletResponse.h"
#import "BpAppointmentList.h"
#import "BpAppointmentLsResponse.h"
#import "BpAppointment.h"
#import "BpMakeAppointmentResponse.h"
#import "Bplogin.h"
#import "BploginResponse.h"
#import "BpMyPoint.h"
#import "BpMyPointResponse.h"

@interface ResponseTranslator : NSObject<IResponseTranslator> {
    
}

-(XMLResponse*)translate:(NSData*) xmlData;
-(XMLResponse*)translateBirthday:(NSData *)xmlData;
-(XMLResponse*)translateCheckUp:(NSData *)xmlData;
-(XMLResponse*)translateFestival:(NSData *)xmlData;
-(XMLResponse*)translateCoupon:(NSData *)xmlData;
-(XMLResponse*)translateEvent:(NSData *)xmlData;
-(XMLResponse*)translateHealthTips:(NSData *)xmlData;
-(XMLResponse*)translatePromotions:(NSData *)xmlData;
-(XMLResponse*)translateOutlet:(NSData *)xmlData;
-(XMLResponse*)translateAppointmentList:(NSData *)xmlData;
-(XMLResponse*)translateMakeAppointment:(NSData *)xmlData;
-(XMLResponse*)translateLogin:(NSData *)xmlData;
-(XMLResponse*)translateMyPoint:(NSData *)xmlData;


@end
