//
//  BpNewAppointmentViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "BpChooseStateViewController.h"
#import "BpSelectBranchViewController.h"
#import "BpAppointmentTypesViewController.h"
#import "BpAppointmentDateTimeViewController.h"
#import "BpAppRemarkViewController.h"
#import "BpContactViewController.h"
#import "BpEmailViewController.h"
#import "BpNoPersonViewController.h"

#import "BpAppointment.h"
#import "BpMakeAppointmentRequest.h"
#import "NetworkHandler.h"
#import "BpMakeAppointmentResponse.h"
#import "LoadingView.h"
#import "BpAppDelegate.h"
#import "BpCustomerNameViewController.h"

@class BpAppointmentViewController;

@interface BpNewAppointmentViewController : UIViewController<NetworkHandlerDelegate,UITextViewDelegate>{
    
}

@property (nonatomic, retain) BpAppointmentViewController *gBpAppointmentViewController;

@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic, retain) NSArray *AppointmentArr;
@property (retain, nonatomic) IBOutlet UITableView *tblNewAppointment;
@property (retain, nonatomic) IBOutlet UILabel *lblSelectedState;
@property (retain, nonatomic) IBOutlet UILabel *lblAppointmentBranch;
@property (retain, nonatomic) IBOutlet UILabel *lblAppointmentType;
@property (retain, nonatomic) IBOutlet UILabel *lblAppointmentDate;
@property (retain, nonatomic) IBOutlet UILabel *lblRemark;
@property (retain, nonatomic) IBOutlet UILabel *lblcontactno;
@property (retain, nonatomic) IBOutlet UILabel *lblemail;
@property (retain, nonatomic) IBOutlet UILabel *lblNoPerson;
@property (retain, nonatomic) IBOutlet UILabel *lblcustomerName;

@property (retain, nonatomic) BpAppointment *BpAppointmentitem;
-(IBAction)SelectStateTapped;
-(IBAction)SelectBranchTapped;
-(IBAction)SelectTypeTapped;
-(IBAction)SelectAppointmentDateTapped;
-(IBAction)SelectRemarkTapped;
-(IBAction)SelectNameTapped;
-(IBAction)SelectContactTapped;
-(IBAction)SelectEmailTapped;
-(IBAction)SelectNoPersonTapped;
-(IBAction)ConfirmAppointmentTapped:(id)sender;
-(void)AlertInsertAll;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollcontent;
@property (retain, nonatomic) IBOutlet UIButton *btnconfirm;

@end
