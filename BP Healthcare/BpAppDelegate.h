//
//  BpAppDelegate.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bplogin.h"
#import "BpStore.h"
//#import "BpAppPushViewController.h"
@class BpViewController;
@class BpAppPushViewController;

@interface BpAppDelegate : UIResponder <UIApplicationDelegate>
{
    UIAlertView *alert;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) BpViewController *viewController;
@property (nonatomic, retain) NSArray *EventArr;
@property (nonatomic, retain) NSArray *AppointmentArr;
@property (nonatomic, retain) Bplogin *loginStatus;
@property (nonatomic, retain) BpStore *storedetail;
@property (nonatomic, retain) NSString *TokenString;
-(void)clearloginStatus;



@end
