//
//  BptestingViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>

@interface BptestingViewController : UIViewController<EKEventViewDelegate>
{
    EKEventViewController *detailViewController;
    EKEventStore *eventStore;
    EKCalendar *defaultCalendar;
    NSMutableArray *eventsList;
}

-(IBAction)addeventtest;
-(IBAction) addEvent:(id)sender;
@property (nonatomic, retain) EKEventViewController *detailViewController;
@property (nonatomic, retain) EKEventStore *eventStore;
@property (nonatomic, retain) EKCalendar *defaultCalendar;
@property (nonatomic, retain) NSMutableArray *eventsList;

@end
