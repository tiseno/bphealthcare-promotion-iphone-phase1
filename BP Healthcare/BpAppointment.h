//
//  BpAppointment.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BpAppointment : NSObject
{
    
}

@property (retain, nonatomic) NSString *resultCount;
@property (retain, nonatomic) NSString *appointmentID;
@property (retain, nonatomic) NSString *appointmentServiceRequest;
@property (retain, nonatomic) NSString *appointmentSort;
@property (retain, nonatomic) NSString *appointmentStatus;

@property (retain, nonatomic) NSString *appointmentNoOfPerson;
@property (retain, nonatomic) NSString *appointmentCreateDate;
@property (retain, nonatomic) NSString *appointmentBookDateTime;
@property (retain, nonatomic) NSString *appointmentBranch;
@property (retain, nonatomic) NSString *userID;

@property (retain, nonatomic) NSString *tokenID;
@property (retain, nonatomic) NSString *adminRemark;
@property (retain, nonatomic) NSString *customerName;
@property (retain, nonatomic) NSString *customerContact;
@property (retain, nonatomic) NSString *customerEmail;

@property (retain, nonatomic) NSString *customerRemark;
@property (retain, nonatomic) NSString *insertResult;


@end



/*<resultCount>string</resultCount>
 <appointmentID>string</appointmentID>
 <appointmentServiceRequest>string</appointmentServiceRequest>
 <appointmentSort>string</appointmentSort>
 <appointmentStatus>string</appointmentStatus>
 
 <appointmentNoOfPerson>string</appointmentNoOfPerson>
 <appointmentCreateDate>string</appointmentCreateDate>
 <appointmentBookDateTime>string</appointmentBookDateTime>
 <appointmentBranch>string</appointmentBranch>
 <userID>string</userID>
 
 <tokenID>string</tokenID>
 <adminRemark>string</adminRemark>
 <customerName>string</customerName>
 <customerContact>string</customerContact>
 <customerEmail>string</customerEmail>
 
 <customerRemark>string</customerRemark>
 <insertResult>string</insertResult>*/
