//
//  BpHealthTips.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpHealthTips.h"

@implementation BpHealthTips
@synthesize resultCount, healthtipsID, healthtipsSort, healthtipsDescp, healthtipsTitle, healthtipsStatus, healthtipsImgPath, healthtipsDateTime;


-(void)dealloc
{
    [healthtipsDateTime release];
    [healthtipsDescp release];
    [healthtipsID release];
    [healthtipsImgPath release];
    [healthtipsSort release];
    [healthtipsStatus release];
    [healthtipsTitle release];
    [resultCount release];
    [super dealloc];
}

@end
