//
//  MTPopupWindow.h
//  KDS
//
//  Created by Tiseno Mac 2 on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AsyncImageView.h"

@interface MTPopupWindow : NSObject
{
    UIView* bgView;
    UIView* bigPanelView;
}

- (id)initWithSuperview:(UIView*)sview andFile:(NSString *)fName;
+(void)showWindowWithHTMLFile:(NSString *)fileName insideView:(UIView*)view;

@end
