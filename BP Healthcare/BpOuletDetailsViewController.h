//
//  BpOuletDetailsViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpAppDelegate.h"
#import "BpStore.h"
#import "Reachability.h"

@interface BpOuletDetailsViewController : UIViewController
{
    
}

@property (nonatomic, retain) NSString *imagePath;
-(void)getStoreDetail;
@property (retain, nonatomic) IBOutlet UILabel *lblBranchName;
@property (retain, nonatomic) IBOutlet UILabel *lblAddress;
@property (retain, nonatomic) IBOutlet UILabel *lblTel;
@property (retain, nonatomic) IBOutlet UILabel *lblFax;
-(IBAction)BranchMapTapped:(id)sender;

@end
