//
//  BpCouponViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpCoupon.h"
#import "BpCouponRequest.h"
#import "BpCouponResponse.h"
#import "NetworkHandler.h"
#import "Reachability.h"
#import "BpCouponCell.h"
#import "LoadingView.h"
#import "AsyncImageView.h"
#import "MTPopupWindow.h"
#import "BpPromotionImageViewController.h"

@interface BpCouponViewController : UIViewController<NetworkHandlerDelegate,UITableViewDelegate, UITableViewDataSource>
{
    BpCouponCell *cell;
}
-(void)getCoupon;
@property (retain, nonatomic) IBOutlet UITableView *tblCoupon;
@property (nonatomic, retain) NSArray *CouponArr;
@property (nonatomic, retain) LoadingView *loadingView;
@property (retain, nonatomic) IBOutlet UIImageView *imgNewAppointment;
@property (retain, nonatomic) IBOutlet UILabel *lblnoItem;

@end
