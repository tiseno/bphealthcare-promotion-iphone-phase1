//
//  BpEventCell.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpEventCell.h"

@implementation BpEventCell
@synthesize lblBodytext, lblTitle, lineColor, imgsmallflag, BpEventcell, imgpicture,lblBodyNoPictext;
@synthesize btnsaveEvent, delegate, lblDatetext, lblDate;
@synthesize imgPath,url;
@synthesize btnEventImage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code 
        
        UIImageView *gimgsmallflag= [[UIImageView alloc]initWithFrame:CGRectMake(30, 10, 20, 21)];        
        //gimgsmallflag.image=[UIImage imageNamed:@"btn_more.png"];
        self.imgsmallflag=gimgsmallflag;
        [gimgsmallflag release];
        [self addSubview:imgsmallflag];
        
        UILabel *glblTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, 200, 21)];
        glblTitle.textAlignment = UITextAlignmentLeft;
        glblTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblTitle.backgroundColor = [UIColor clearColor];
        self.lblTitle = glblTitle;
        [glblTitle release];
        [self addSubview:lblTitle];
        
        UIImageView *gimgpicture= [[UIImageView alloc]initWithFrame:CGRectMake(10, 50, 40, 40)];        
        //gimgsmallflag.image=[UIImage imageNamed:@"btn_more.png"];
        self.imgpicture=gimgpicture;
        [gimgpicture release];
        [self addSubview:imgpicture];
        
        
        
        
        /*UIImageView *imgbtnenlargeimg = [[UIImageView alloc] init];
        imgbtnenlargeimg.imageURL = [NSURL URLWithString:self.imgPath];
        
        NSLog(@"imgPath--------->%@",self.imgPath);
        UIButton *gbtnenlargeimg = [[UIButton alloc] initWithFrame:CGRectMake(255, 35, 60, 63)];
        [gbtnenlargeimg setBackgroundImage:imgbtnenlargeimg.image forState:UIControlStateNormal];
        [gbtnenlargeimg addTarget:self action:@selector(BtnSaveEventTapped:) forControlEvents:UIControlEventTouchDown];
        self.btnenlargeimg = gbtnenlargeimg;
        [gbtnenlargeimg release];
        [self addSubview:gbtnenlargeimg]; */
        
        
        
        
        UILabel *glblBodytext = [[UILabel alloc] initWithFrame:CGRectMake(80, 60, 455, 45)];
        glblBodytext.textAlignment = UITextAlignmentLeft;
        glblBodytext.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
        glblBodytext.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblBodytext.backgroundColor = [UIColor clearColor];
        self.lblBodytext = glblBodytext;
        [glblBodytext release];
        [self addSubview:lblBodytext];
        
        UILabel *glblDatetext = [[UILabel alloc] initWithFrame:CGRectMake(80, 100, 205, 45)];
        glblDatetext.textAlignment = UITextAlignmentLeft;
        glblDatetext.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
        glblDatetext.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblDatetext.backgroundColor = [UIColor clearColor];
        self.lblDatetext = glblDatetext;
        [glblDatetext release];
        [self addSubview:lblDatetext];
        
        UILabel *glblDate = [[UILabel alloc] initWithFrame:CGRectMake(80, 100, 205, 45)];
        glblDate.textAlignment = UITextAlignmentLeft;
        glblDate.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
        glblDate.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblDate.backgroundColor = [UIColor clearColor];
        self.lblDate = glblDate;
        [glblDate release];
        [self addSubview:lblDate];
        
        UILabel *glblBodyNoPictext = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 455, 45)];
        glblBodyNoPictext.textAlignment = UITextAlignmentLeft;
        glblBodyNoPictext.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
        glblBodyNoPictext.backgroundColor = [UIColor clearColor];
        self.lblBodyNoPictext = glblBodyNoPictext;
        [glblBodyNoPictext release];
        [self addSubview:lblBodyNoPictext];
        
        UIImage *imgbtnsaveEvent = [UIImage imageNamed:@"icon_add_calendar.png"];
        UIButton *gbtnsaveEvent = [[UIButton alloc] initWithFrame:CGRectMake(255, 10, 60, 63)];
        [gbtnsaveEvent setBackgroundImage:imgbtnsaveEvent forState:UIControlStateNormal];
        [gbtnsaveEvent addTarget:self action:@selector(BtnSaveEventTapped:) forControlEvents:UIControlEventTouchDown];
        self.btnsaveEvent = gbtnsaveEvent;
        [gbtnsaveEvent release];
        [self addSubview:btnsaveEvent];
        
        
        //UIImage *imgbtnsaveEvent = [UIImage imageNamed:@"icon_add_calendar.png"];
        UIButton *gbtnEventImage = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, 250, 70)];
        //[gbtnEventImage setBackgroundImage:imgbtnsaveEvent forState:UIControlStateNormal];
        [gbtnEventImage setBackgroundColor:[UIColor clearColor]];
        
        [gbtnEventImage addTarget:self action:@selector(BtnEventImageTapped:) forControlEvents:UIControlEventTouchDown];
        self.btnEventImage = gbtnEventImage;
        [gbtnsaveEvent release];
        [self addSubview:btnEventImage];
        
        
        [self performSelector:@selector(resizeimage) withObject:nil afterDelay:1.0];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
 
-(IBAction)BtnEventImageTapped:(id)sender
{
    [self.delegate imgurlTapped:BpEventcell.eventsImgPath];
}

-(IBAction)BtnSaveEventTapped:(id)sender
{
    [self.delegate addEventTapped:(BpEvent*)BpEventcell];
}

-(void)resizeimage
{
    if(self.url != nil)
    {
        float width = self.imgpicture.image.size.width * (float)65 / self.imgpicture.image.size.height;
        if(isnan(width))
            [self performSelector:@selector(resizeimage) withObject:nil afterDelay:1.0];
        else{
            //[self.imgpicture setFrame:CGRectMake(320 - width - 5, 3, width, 55.0)];
            
            
            CGSize maxsizetitle = CGSizeMake(180, 9999);
            UIFont *thefonttitle = [UIFont fontWithName:@"Helvetica-Bold" size:17];
            CGSize textsizetitle = [self.lblTitle.text sizeWithFont:thefonttitle constrainedToSize:maxsizetitle lineBreakMode:UILineBreakModeWordWrap];
            
            self.lblTitle.numberOfLines=20;
            self.lblTitle.frame=CGRectMake(80, 10, 180, textsizetitle.height);
            self.lblTitle.lineBreakMode=UILineBreakModeWordWrap;
            //self.lblTitle.text=classBpHealthTips.healthtipsTitle;
            
            float height = self.imgpicture.image.size.height * (float)65 / self.imgpicture.image.size.width;
            //self.imgpicture.frame = CGRectMake(10, textsizetitle.height+50, 65, height);
            self.imgpicture.frame = CGRectMake(10, textsizetitle.height+28, 65, height);
        }
    }
}

-(void)dealloc
{
    [btnEventImage release];
    [url release];
    [imgPath release];
    [lblDate release];
    [lblDatetext release];
    [delegate release];
    [btnsaveEvent release];
    [lblBodyNoPictext release];
    [imgpicture release];
    [lineColor release];
    [BpEventcell release];
    [imgsmallflag release];
    [lblTitle release];
    [lblBodytext release];
    [super dealloc];
}

@end
