//
//  BpCheckUpCell.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpCheckUp.h"
@interface BpCheckUpCell : UITableViewCell{
    
}
@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic,retain) UILabel *lblTitle;
@property (nonatomic, retain) UILabel *lblBodytext;
@property (nonatomic, retain) UILabel *lblDate;
@property (nonatomic, retain) UILabel *lblDateText;
@property (nonatomic, retain) UILabel *lblRemark;
@property (nonatomic, retain) UILabel *lblRemarktext;
@property (nonatomic, retain) UIImageView *imgsmallflag;

@property (nonatomic, retain) BpCheckUp *BpCheckUpcell;

@end
