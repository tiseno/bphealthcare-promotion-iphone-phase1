//
//  BpStore.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

//#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "BpLocation.h"
#import <MapKit/MapKit.h> 
#import <CoreLocation/CoreLocation.h>
#import "BpOutlet.h"

@interface BpStore : BpOutlet <MKAnnotation>
{
    NSString *storeName;
	NSString *description;
	NSString *phoneNumber;
	NSString *faxNumber;
	NSString *storeAddress;
	NSString *storeMainImage;
	//These images are only displayed in the details pages. The only one accessible outside is main image;
	NSString *storeFirstImage;
	NSString *storeSecondImage;
	NSString *storeThirdImage;
	double storeRating;
	NSInteger numberOfRatingVotes;
	BpLocation *location;
    
    NSMutableArray *facilityArray;
}
@property (nonatomic,copy) NSString *storeName;
@property (nonatomic,retain) NSString *description;
@property (nonatomic,copy) NSString *phoneNumber;
@property (nonatomic,copy) NSString *faxNumber;
@property (nonatomic,copy) NSString *storeAddress;
@property (nonatomic,copy) NSString *storeMainImage;
@property (nonatomic,copy) NSString *storeFirstImage;
@property (nonatomic,copy) NSString *storeSecondImage;
@property (nonatomic,copy) NSString *storeThirdImage;
@property (nonatomic,retain) BpLocation *location;
@property (nonatomic) double storeRating;
@property (nonatomic) NSInteger numberOfRatingVotes;
@property (nonatomic, retain) NSMutableArray *facilityArray;

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
- (NSString *) title;

@end

