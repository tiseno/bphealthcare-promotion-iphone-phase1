//
//  BploginRequest.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BploginRequest.h"

@implementation BploginRequest
@synthesize icno;

-(id)initWithisicno:(NSString *)iicno
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"login";
        self.requestType=WebserviceRequest;
        self.icno=iicno;
    }
    return self;
}

-(void)dealloc
{
    [icno release];
    [super dealloc];
}

-(NSString*) generateHTTPPostMessage
{
    
    NSString *postMsg = [NSString stringWithFormat:@"icno=%@",self.icno];
    return postMsg;
} 

@end
