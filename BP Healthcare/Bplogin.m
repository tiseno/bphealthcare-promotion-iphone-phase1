//
//  Bplogin.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Bplogin.h"

@implementation Bplogin
@synthesize resultCount, LoginStatus, ICNO;

-(void)dealloc
{
    [ICNO release];
    [LoginStatus release];
    [resultCount release];
    [super dealloc];
}

@end



/*<resultCount>string</resultCount>
 <LoginStatus>string</LoginStatus>
 <ICNO>string</ICNO>*/