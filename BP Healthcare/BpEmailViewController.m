//
//  BpEmailViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpEmailViewController.h"

@interface BpEmailViewController ()

@end

@implementation BpEmailViewController
@synthesize btnemaildone;
@synthesize txtEmail;
@synthesize gBpNewAppointmentViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpEmailViewController *gBpEmailViewController=[[BpEmailViewController alloc] initWithNibName:@"BpEmailViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpEmailViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];    
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(DoneEmailtapped:) forControlEvents:UIControlEventTouchUpInside];   
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: rightButton] autorelease]; 
    
    gBpEmailViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton release];
    
    [leftButton release];
    [gBpEmailViewController release];
    
    [[txtEmail layer] setBorderWidth:1];
	[[txtEmail layer] setCornerRadius:10];
    txtEmail.layer.borderColor=[UIColor colorWithRed:(170/255.0) green:(170/255.0) blue:(170/255.0) alpha:1.0].CGColor;
    [txtEmail becomeFirstResponder];
    
    btnemaildone.hidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
}

- (void)viewDidUnload
{
    [self setTxtEmail:nil];
    [self setBtnemaildone:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    heightOfEditedView = textView.frame.size.height;
    heightOffset = textView.frame.origin.y+280;
    
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    
}

-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    //iskeyboardDisplayed=NO;
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]){
        [txtEmail resignFirstResponder];
        return NO;
    }else{
        return YES;
    }
}

-(IBAction)DoneEmailtapped:(id)sender
{
    UILabel *lblServiceTypeSelected=(UILabel*)[self.gBpNewAppointmentViewController lblemail];
    
    lblServiceTypeSelected.text=txtEmail.text;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc 
{
    [gBpNewAppointmentViewController release];
    [txtEmail release];
    [btnemaildone release];
    [super dealloc];
}
@end
