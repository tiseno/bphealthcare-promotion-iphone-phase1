//
//  BpOuletDetailsViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpOuletDetailsViewController.h"

@interface BpOuletDetailsViewController ()

@end

@implementation BpOuletDetailsViewController
@synthesize lblBranchName;
@synthesize lblAddress;
@synthesize lblTel;
@synthesize lblFax;
@synthesize imagePath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpOuletDetailsViewController *gBpOuletDetailsViewController=[[BpOuletDetailsViewController alloc] initWithNibName:@"BpOuletDetailsViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpOuletDetailsViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpOuletDetailsViewController release];
    
    
    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    lblBranchName.text=appDelegate.storedetail.BranchName;
    
    NSString *Aaddress=[[NSString alloc]initWithFormat:@"%@ %@ %@",appDelegate.storedetail.Address_1, appDelegate.storedetail.Address_2, appDelegate.storedetail.Address_3];
    lblAddress.text=Aaddress;
    lblTel.text=appDelegate.storedetail.Tel;
    lblFax.text=appDelegate.storedetail.Fax;
    //[self getStoreDetail];
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)NetworkStatus
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus==NotReachable);
}

-(IBAction)BranchMapTapped:(id)sender
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        
        NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?saddr=%@,%@&daddr=%@,%@", appDelegate.storedetail.currentLatitude, appDelegate.storedetail.currentLongitude, appDelegate.storedetail.Latitude, appDelegate.storedetail.Longitude]; 
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
    
}

-(void)getStoreDetail
{
    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSLog(@"appDelegate.storedetail.BranchName---->%@",appDelegate.storedetail.BranchName);
    
    NSLog(@"appDelegate.currentLongitude---->%@",appDelegate.storedetail.currentLongitude);
    NSLog(@"appDelegate.currentLatitude---->%@",appDelegate.storedetail.currentLatitude);
    
    NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?saddr=%@,%@&daddr=%@,%@", appDelegate.storedetail.currentLatitude, appDelegate.storedetail.currentLongitude, appDelegate.storedetail.Latitude, appDelegate.storedetail.Longitude]; 
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

}

- (void)viewDidUnload
{
    [self setLblBranchName:nil];
    [self setLblAddress:nil];
    [self setLblTel:nil];
    [self setLblFax:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)dealloc
{
    [imagePath release];
    [lblBranchName release];
    [lblAddress release];
    [lblTel release];
    [lblFax release];
    [super dealloc];
    
}
@end
