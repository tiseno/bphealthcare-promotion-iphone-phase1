//
//  BpFestivalResponse.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpFestivalResponse.h"

@implementation BpFestivalResponse
@synthesize FestivalArr;

-(void)dealloc
{
    [FestivalArr release];
    [super dealloc];
}

@end
