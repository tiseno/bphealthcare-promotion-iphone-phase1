//
//  BptestingViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BptestingViewController.h"

@interface BptestingViewController ()

@end

@implementation BptestingViewController
@synthesize defaultCalendar, detailViewController, eventsList, eventStore;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BptestingViewController *gBpMainViewController=[[BptestingViewController alloc] initWithNibName:@"BptestingViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    //[leftButton setUserInteractionEnabled:NO];
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpMainViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpMainViewController release];
    
    
    
    
    self.eventStore = [[EKEventStore alloc] init];
    self.eventsList = [[NSMutableArray alloc] initWithArray:0];
    // Get the default calendar from store.
    self.defaultCalendar = [self.eventStore defaultCalendarForNewEvents];
    
    // create an Add button
    UIBarButtonItem *addButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bell.png" ] style:UIBarButtonItemStylePlain target:self action:@selector(addEvent:)];
    //UIBarButtonItem *addButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(addEvent:)];
    self.navigationItem.rightBarButtonItem = addButtonItem;
    [addButtonItem release];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)addeventtest
{
    //EKEventStore *eventStore = [[EKEventStore alloc] init];
    /*NSError *err;
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
    event.title = @"title";
    event.startDate = [NSDate date];
    //event.endDate = somedate;
    [event setCalendar:[eventStore defaultCalendarForNewEvents]];
    [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
    
    EKEventViewController *detailViewController;
     EKEventStore *eventStore;
     eventStore = [[EKEventStore alloc] init]; 
     EKEvent *event = [EKEvent eventWithEventStore:eventStore]; 
     event.title = @"Test Event"; 
     //event.startDate = copyofnextappdt; 
     //event.endDate = copyofnextappdt;
     [event setCalendar:[eventStore defaultCalendarForNewEvents]];
     NSError *err;
     [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
    NSLog(@"hello~");*/
}

#pragma mark -

#pragma mark Add a new Event
-(IBAction) addEvent:(id)sender {
    
    self.eventStore = [[EKEventStore alloc] init];
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    //  event.title =@"Whatever you want your title to be";
    event.title = @"hello There~";// self.currentTitle;
    
    /**/NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yyyy hh:mm a"];
    NSDate *myDate = [df dateFromString: @"16/08/2012 02:00  pm"];
    //startDate=@"";
    
    event.startDate=myDate;
    event.endDate=[df dateFromString: @"16/08/2012 02:00  pm"];
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    event.allDay = YES;
    
    //EKEventViewController *gdetailViewController=[[EKEventViewController alloc] initWithNibName:nil bundle:nil];
    EKEventEditViewController *addController = [[EKEventEditViewController alloc] initWithNibName:nil bundle:nil];
    addController.event = event;
    addController.eventStore = self.eventStore;
    [self presentModalViewController:addController animated:YES];
    //[self.navigationController pushViewController:addController animated:YES];
    //[self.navigationController popToViewController:addController animated:YES];
    
    addController.editViewDelegate = self;
    [addController release];
    
}


#pragma mark -
#pragma mark EKEventEditViewDelegate

// Overriding EKEventEditViewDelegate method to update event store according to user actions.
- (void)eventEditViewController:(EKEventEditViewController *)controller 
          didCompleteWithAction:(EKEventEditViewAction)action {
    
    NSError *error = nil;
    EKEvent *thisEvent = controller.event;
    
    switch (action) {
        case EKEventEditViewActionCanceled:
            // Edit action canceled, do nothing. 
            break;
            
        case EKEventEditViewActionSaved:
            // When user hit "Done" button, save the newly created event to the event store, 
            // and reload table view.
            // If the new event is being added to the default calendar, then update its 
            // eventsList.
            if (self.defaultCalendar ==  thisEvent.calendar) {
                [self.eventsList addObject:thisEvent];
            }
            [controller.eventStore saveEvent:controller.event span:EKSpanThisEvent error:&error];
            //  [self.tableView reloadData];
            break;
            
        case EKEventEditViewActionDeleted:
            // When deleting an event, remove the event from the event store, 
            // and reload table view.
            // If deleting an event from the currenly default calendar, then update its 
            // eventsList.
            if (self.defaultCalendar ==  thisEvent.calendar) {
                [self.eventsList removeObject:thisEvent];
            }
            [controller.eventStore removeEvent:thisEvent span:EKSpanThisEvent error:&error];
            //[self.tableView reloadData];
            break;
            
        default:
            break;
    }
    // Dismiss the modal view controller
    /*BptestingViewController *gBpMainViewController=[[BptestingViewController alloc] initWithNibName:@"BptestingViewController" bundle:nil];
    for (UIView* uiview in self.view.subviews) {
        [uiview removeFromSuperview];
    }
    didMoveToParentViewController:gBpMainViewController ];//*/
    [controller  dismissModalViewControllerAnimated:YES];
    
}


// Set the calendar edited by EKEventEditViewController to our chosen calendar - the default calendar.
- (EKCalendar *)eventEditViewControllerDefaultCalendarForNewEvents:(EKEventEditViewController *)controller {
    EKCalendar *calendarForEdit = self.defaultCalendar;
    return calendarForEdit;
}

@end
