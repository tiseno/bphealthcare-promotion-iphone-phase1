//
//  BpEvent.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BpEvent : NSObject{
    
}

@property (nonatomic,retain) NSString *resultCount;
@property (nonatomic,retain) NSString *eventsID;
@property (nonatomic,retain) NSString *eventsTitle;
@property (nonatomic,retain) NSString *eventsDescp;
@property (nonatomic,retain) NSString *eventsSort;
@property (nonatomic,retain) NSString *eventsStatus;
@property (nonatomic,retain) NSString *eventsDateTime;
@property (nonatomic,retain) NSString *eventsImgPath;
@property (nonatomic,retain) NSString *eventsCreateDate;

@end


