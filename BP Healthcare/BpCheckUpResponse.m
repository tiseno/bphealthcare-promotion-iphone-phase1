//
//  BpCheckUpResponse.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpCheckUpResponse.h"

@implementation BpCheckUpResponse
@synthesize CheckUpArr;

-(void)dealloc
{
    [CheckUpArr release];
    [super dealloc];
}
@end
