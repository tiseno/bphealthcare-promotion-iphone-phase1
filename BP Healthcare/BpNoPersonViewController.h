//
//  BpNoPersonViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@class BpNewAppointmentViewController;

@interface BpNoPersonViewController : UIViewController{
    int heightOfEditedView;
    int heightOfEditedArea;
    int heightOffset;
    NSMutableArray *person;
}

@property (retain, nonatomic) IBOutlet UITextView *txtNoPerson;
@property (nonatomic, retain) NSString *Noperson;
@property (nonatomic, retain) BpNewAppointmentViewController *gBpNewAppointmentViewController;
-(IBAction)DoneNoPersontapped:(id)sender;
@property (retain, nonatomic) IBOutlet UIPickerView *dropdown;
@property (retain, nonatomic) IBOutlet UILabel *lblNoofperson;

@end
