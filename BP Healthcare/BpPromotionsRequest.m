//
//  BpPromotionsRequest.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpPromotionsRequest.h"

@implementation BpPromotionsRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"retrievePromotionList";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}

@end

