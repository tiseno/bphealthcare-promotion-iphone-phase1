//
//  BpCouponViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpCouponViewController.h"

@interface BpCouponViewController ()

@end

@implementation BpCouponViewController
@synthesize tblCoupon;
@synthesize  CouponArr, loadingView;
@synthesize imgNewAppointment, lblnoItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpCouponViewController *gBpCouponViewController=[[BpCouponViewController alloc] initWithNibName:@"BpCouponViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpCouponViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpCouponViewController release];
    
    [self getCoupon];
 
    lblnoItem.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    //self.tblCoupon.rowHeight = 130;
}

- (void)viewDidUnload
{
    [self setTblCoupon:nil];
    [self setImgNewAppointment:nil];
    [self setLblnoItem:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getCoupon
{
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    BpCouponRequest *requestset= [[BpCouponRequest alloc] init];
    
    [networkHandler setDelegate:self];
    [networkHandler request:requestset];
    [requestset release];
    [networkHandler release];
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpCouponResponse class]])
    {
        
        //NSString *strMessage=[NSString stringWithFormat:@"%@",((BpCheckUpResponse*)responseMessage).CheckUpArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpCouponResponse*)responseMessage).CouponArr;
        self.CouponArr=msgArr;
        for (BpCoupon *item in msgArr) {
            //birthitem.resultCount;
            /*
            NSLog(@"birthitem.resultCount--->%@",item.resultCount);
            NSLog(@"birthitem.festivalImgPath--->%@",item.couponSort);
            NSLog(@"birthitem.festivalID--->%@",item.couponStatus);
            
            NSLog(@"birthitem.festivalDescp--->%@",item.couponTitle);
            NSLog(@"birthitem.festivalCreateDate--->%@",item.couponStatus);
            NSLog(@"birthitem.festivalSort--->%@",item.couponImgPath);
            
            NSLog(@"birthitem.festivalStatus--->%@",item.couponID);
            NSLog(@"birthitem.festivalTitle--->%@",item.couponDateTime);
            */
            
            if (![item.resultCount isEqualToString:@"0"])
            {
                
                imgNewAppointment.hidden=YES;
                lblnoItem.hidden=YES;
                
            }else
            {
                imgNewAppointment.hidden=NO;
                lblnoItem.hidden=NO;
            }
            
        }
        
        [tblCoupon reloadData];
        [self.loadingView removeView];
        
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    } 
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.CouponArr count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIDentifier=@"Cell";
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIDentifier];
    cell= (BpCouponCell*)[tableView dequeueReusableCellWithIdentifier:cellIDentifier];
    if (cell == nil) {
        cell = [[[BpCouponCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIDentifier]autorelease];
    }
    BpCoupon* classObject=[self.CouponArr objectAtIndex:indexPath.row];
    
    cell.BpCouponcell=classObject;
    
    cell.imgsmallflag.image=[UIImage imageNamed:@"icon_coupons_2.png"];;
    
    
    if (classObject.couponImgPath.length!=0) 
    {
        
        /*=============*/
        
        CGSize maxsizetitle = CGSizeMake(220, 9999);
        UIFont *thefonttitle = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        CGSize textsizetitle = [classObject.couponTitle sizeWithFont:thefonttitle constrainedToSize:maxsizetitle lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblTitle.numberOfLines=20;
        cell.lblTitle.frame=CGRectMake(80, 10, 220, textsizetitle.height);
        cell.lblTitle.lineBreakMode=UILineBreakModeWordWrap;
        cell.lblTitle.text=classObject.couponTitle;
        
        CGSize maxsize = CGSizeMake(220, 9999);
        UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:12];
        CGSize textsize = [classObject.couponDescp sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblBodytext.numberOfLines=20;
        cell.lblBodytext.frame=CGRectMake(80, textsizetitle.height+20, 220, textsize.height);
        cell.lblBodytext.lineBreakMode=UILineBreakModeWordWrap;
        cell.lblBodytext.text=classObject.couponDescp;
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsizetitle.height+textsize.height+30);
        
        /*=============*/
        
        
        //cell.imgpicture.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imagename]]];
        
        
        NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/coupon/%@",classObject.couponImgPath];
        cell.url=imagename;
        cell.imgpicture.imageURL = [NSURL URLWithString:imagename];
        
        /*if(cell.imgpicture.image != nil)
        {
            float height = cell.imgpicture.image.size.height * (float)55 / cell.imgpicture.image.size.width;
            //[cell.imgpicture setFrame:CGRectMake(10, 50, 65, height)];
            //NSLog(@"height--->%f",height);
            cell.imgpicture.frame = CGRectMake(10, textsizetitle.height+23, 65, height);
            
        }else 
        {
            NSLog(@"nilll");
        }*/
        
    }else 
    {
        /*=============*/
        CGSize maxsize = CGSizeMake(220, 9999);
        UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:12];
        CGSize textsize = [classObject.couponDescp sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblBodyNoPictext.numberOfLines=20;
        cell.lblBodyNoPictext.frame=CGRectMake(10, 35, 220, textsize.height+50);
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsize.height+100);
        
        /*=============*/
        
        cell.lblBodyNoPictext.text=classObject.couponDescp;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    //[self.loadingView removeView];
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    cell = (BpCouponCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BpCouponCell *selectedcell= (BpCouponCell*)[tableView cellForRowAtIndexPath:indexPath]; 
    
    
    //BpPromotions* classBpPromotions=[self.promtArr objectAtIndex:indexPath.row];
    
    //cell.BpPromotionscell=classBpPromotions;
    if (![selectedcell.BpCouponcell.couponImgPath isEqualToString:@"none"])
    {
        
        NSString *itemimg=[selectedcell.BpCouponcell.couponImgPath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/coupon/a%@",itemimg];
        
        BpPromotionImageViewController *gBpPromotionImageViewController=[[BpPromotionImageViewController alloc] initWithNibName:@"BpPromotionImageViewController" bundle:nil];
        gBpPromotionImageViewController.title=@"Coupons";
        gBpPromotionImageViewController.imgPath=imagename;
        [self.navigationController pushViewController:gBpPromotionImageViewController animated:YES];
        [gBpPromotionImageViewController release];
        
    }else
    {
        NSLog(@"no image");
    }
    
    
    /*
    [MTPopupWindow showWindowWithHTMLFile:imagename insideView:self.view];
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
    [self performSelector:@selector(showloadingView) withObject:nil afterDelay:2];
     */
}

-(void)showloadingView
{
    [self.loadingView removeView];
}

- (void)dealloc 
{
    [loadingView release];
    [CouponArr release];
    [tblCoupon release];
    [imgNewAppointment release];
    [lblnoItem release];
    [super dealloc];
}
@end
