//
//  BpPromotionImageViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 11/21/12.
//
//

#import <UIKit/UIKit.h>
#import "LoadingView.h"
#import "AsyncImageView.h"

@interface BpPromotionImageViewController : UIViewController<UIScrollViewDelegate>{
    
}
@property (nonatomic, retain) NSString *imgPath;
@property (retain, nonatomic) IBOutlet UIImageView *promotionimgView;
@property (retain, nonatomic) IBOutlet UIScrollView *imgscrollView;
@property (nonatomic, retain) LoadingView *loadingView;

@end
