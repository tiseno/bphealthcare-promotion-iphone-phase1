//
//  AppointmentListDelegate.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol AppointmentListDelegate <NSObject>
//-(void)editSaleOrderForEditButton;
//-(void)reloadNewOrderTable;
//-(void)editSaleOrderPDFButton:(SalesOrder*)isaleOrder SalesPersonCode:(SalesPerson*)isalesPersonCode;
//-(void)editSaleOrderPDFButton:(SalesOrder *)isaleOrder SalesPersonCode:(NSString *)isalesPersonCode;
-(void)addAppointmentTapped;

@end