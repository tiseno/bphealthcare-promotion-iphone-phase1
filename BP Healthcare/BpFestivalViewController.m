//
//  BpFestivalViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpFestivalViewController.h"

@interface BpFestivalViewController ()

@end

@implementation BpFestivalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpFestivalViewController *gBpFestivalViewController=[[BpFestivalViewController alloc] initWithNibName:@"BpFestivalViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpFestivalViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpFestivalViewController release];
    
    [self getFestival];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getFestival
{
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    BpFestivalRequest *requestset= [[BpFestivalRequest alloc] init];
    
    [networkHandler setDelegate:self];
    [networkHandler request:requestset];
    [requestset release];
    [networkHandler release];
}
-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpFestivalResponse class]])
    {
        
        //NSString *strMessage=[NSString stringWithFormat:@"%@",((BpCheckUpResponse*)responseMessage).CheckUpArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpFestivalResponse*)responseMessage).FestivalArr;

        for (BpFestival *item in msgArr) {
            //birthitem.resultCount;
            NSLog(@"birthitem.resultCount--->%@",item.resultCount);
            NSLog(@"birthitem.festivalImgPath--->%@",item.festivalImgPath);
            NSLog(@"birthitem.festivalID--->%@",item.festivalID);
            
            NSLog(@"birthitem.festivalDescp--->%@",item.festivalDescp);
            NSLog(@"birthitem.festivalCreateDate--->%@",item.festivalCreateDate);
            NSLog(@"birthitem.festivalSort--->%@",item.festivalSort);
            
            NSLog(@"birthitem.festivalStatus--->%@",item.festivalStatus);
            NSLog(@"birthitem.festivalTitle--->%@",item.festivalTitle);
            
            
        }
        
    }
    
}


@end
