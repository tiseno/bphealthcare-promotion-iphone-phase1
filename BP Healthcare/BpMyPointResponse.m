//
//  BpMyPointResponse.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpMyPointResponse.h"

@implementation BpMyPointResponse
@synthesize MyPointArr;

-(void)dealloc
{
    [MyPointArr release];
    [super dealloc];
}

@end
