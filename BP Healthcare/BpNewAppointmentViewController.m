//
//  BpNewAppointmentViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpNewAppointmentViewController.h"

@interface BpNewAppointmentViewController ()

@end

@implementation BpNewAppointmentViewController
@synthesize scrollcontent;
@synthesize lblSelectedState;
@synthesize lblAppointmentBranch;
@synthesize lblAppointmentType;
@synthesize lblAppointmentDate;
@synthesize lblRemark;
@synthesize lblcontactno;
@synthesize lblemail;
@synthesize lblNoPerson;
@synthesize lblcustomerName;
@synthesize tblNewAppointment;
@synthesize BpAppointmentitem;
@synthesize AppointmentArr;
@synthesize loadingView;
@synthesize gBpAppointmentViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        
        NSLog(@"code for 4-inch screen");
        [self fView];
    } else {
        // code for 3.5-inch screen
        NSLog(@"code for 3.5-inch screen");
        [self tView];
    }
    
    
    BpNewAppointmentViewController *gBpNewAppointmentViewController=[[BpNewAppointmentViewController alloc] initWithNibName:@"BpNewAppointmentViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpNewAppointmentViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpNewAppointmentViewController release];
    
    tblNewAppointment.layer.cornerRadius=10;
    tblNewAppointment.layer.borderColor = [UIColor grayColor].CGColor;
    tblNewAppointment.layer.borderWidth = 1;
    tblNewAppointment.hidden=YES;
    
    
    //self.scrollcontent.contentSize=CGSizeMake(self.scrollcontent.frame.size.width, 385);

    //self.scrollcontent.frame=CGRectMake(14, 13, 293, 337);
}

-(void)fView
{
    self.scrollcontent.frame=CGRectMake(14, 13, 293, 480);
    //[self.view addSubview:self.scrollcontent];
    self.btnconfirm.frame=CGRectMake(20, 450, 280, 47);
}
-(void)tView
{
    self.scrollcontent.frame=CGRectMake(14, 101, 293, 337);
    //[self.view addSubview:self.scrollcontent];
    self.btnconfirm.frame=CGRectMake(20, 362, 280, 47);
    self.scrollcontent.contentSize=CGSizeMake(self.scrollcontent.frame.size.width, 385);
}

- (void)viewDidUnload
{
    [self setTblNewAppointment:nil];
    [self setLblSelectedState:nil];
    [self setLblAppointmentBranch:nil];
    [self setLblAppointmentType:nil];
    [self setLblAppointmentDate:nil];
    [self setLblRemark:nil];
    [self setLblcontactno:nil];
    [self setLblemail:nil];
    [self setLblNoPerson:nil];
    [self setScrollcontent:nil];
    [self setLblcustomerName:nil];
    [self setBtnconfirm:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(IBAction)SelectStateTapped
{
    BpChooseStateViewController *gBpChooseStateViewController=[[BpChooseStateViewController alloc] initWithNibName:@"BpChooseStateViewController" bundle:nil];
    
    gBpChooseStateViewController.title=@"New Appointment";
    gBpChooseStateViewController.gBpNewAppointmentViewController=self;
    [self.navigationController pushViewController:gBpChooseStateViewController animated:YES];
    [gBpChooseStateViewController release];
}

-(IBAction)SelectBranchTapped
{
    //if ([lblSelectedState.text length]!= 0) {
        
        BpSelectBranchViewController *gBpSelectBranchViewController=[[BpSelectBranchViewController alloc] initWithNibName:@"BpSelectBranchViewController" bundle:nil];
        
        gBpSelectBranchViewController.title=@"New Appointment";
        gBpSelectBranchViewController.selectedState=lblSelectedState.text;
        gBpSelectBranchViewController.gBpNewAppointmentViewController=self;
        [self.navigationController pushViewController:gBpSelectBranchViewController animated:YES];
        [gBpSelectBranchViewController release];
        
    /*}else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Please select a state.      Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
    }*/
    
}

-(IBAction)SelectTypeTapped
{
    BpAppointmentTypesViewController *gBpAppointmentTypesViewController=[[BpAppointmentTypesViewController alloc] initWithNibName:@"BpAppointmentTypesViewController" bundle:nil];
    
    gBpAppointmentTypesViewController.title=@"New Appointment";
    //gBpAppointmentTypesViewController.selectedState=lblSelectedState.text;
    
    gBpAppointmentTypesViewController.SelectedBranch=lblAppointmentBranch.text;
    gBpAppointmentTypesViewController.gBpNewAppointmentViewController=self;
    [self.navigationController pushViewController:gBpAppointmentTypesViewController animated:YES];
    [gBpAppointmentTypesViewController release];
}

-(IBAction)SelectAppointmentDateTapped
{
    BpAppointmentDateTimeViewController *gBpAppointmentDateTimeViewController=[[BpAppointmentDateTimeViewController alloc] initWithNibName:@"BpAppointmentDateTimeViewController" bundle:nil];
    
    gBpAppointmentDateTimeViewController.title=@"New Appointment";
    //gBpAppointmentTypesViewController.selectedState=lblSelectedState.text;
    gBpAppointmentDateTimeViewController.gBpNewAppointmentViewController=self;
    [self.navigationController pushViewController:gBpAppointmentDateTimeViewController animated:YES];
    [gBpAppointmentDateTimeViewController release];
}

-(IBAction)SelectRemarkTapped
{
    BpAppRemarkViewController *gBpAppRemarkViewController=[[BpAppRemarkViewController alloc] initWithNibName:@"BpAppRemarkViewController" bundle:nil];
    
    gBpAppRemarkViewController.title=@"New Appointment";
    //gBpAppointmentTypesViewController.selectedState=lblSelectedState.text;
    gBpAppRemarkViewController.gBpNewAppointmentViewController=self;
    gBpAppRemarkViewController.txtRemark.delegate=self;
    [self.navigationController pushViewController:gBpAppRemarkViewController animated:YES];
    [gBpAppRemarkViewController release];
}

-(IBAction)SelectContactTapped
{
    BpContactViewController *gBpContactViewController=[[BpContactViewController alloc] initWithNibName:@"BpContactViewController" bundle:nil];
    
    gBpContactViewController.title=@"New Appointment";
    //gBpAppointmentTypesViewController.selectedState=lblSelectedState.text;
    gBpContactViewController.gBpNewAppointmentViewController=self;
    [self.navigationController pushViewController:gBpContactViewController animated:YES];
    [gBpContactViewController release];
    
}

-(IBAction)SelectEmailTapped
{
    BpEmailViewController *gBpEmailViewController=[[BpEmailViewController alloc] initWithNibName:@"BpEmailViewController" bundle:nil];
    
    gBpEmailViewController.title=@"New Appointment";
    //gBpAppointmentTypesViewController.selectedState=lblSelectedState.text;
    gBpEmailViewController.gBpNewAppointmentViewController=self;
    [self.navigationController pushViewController:gBpEmailViewController animated:YES];
    [gBpEmailViewController release];
    
}

-(IBAction)SelectNoPersonTapped
{
        
    BpNoPersonViewController *gBpNoPersonViewController=[[BpNoPersonViewController alloc] initWithNibName:@"BpNoPersonViewController" bundle:nil];
    
    gBpNoPersonViewController.title=@"New Appointment";
    //gBpAppointmentTypesViewController.selectedState=lblSelectedState.text;
    gBpNoPersonViewController.gBpNewAppointmentViewController=self;
    [self.navigationController pushViewController:gBpNoPersonViewController animated:YES];
    [gBpNoPersonViewController release];
    
}

-(IBAction)SelectNameTapped
{
    BpCustomerNameViewController *gBpCustomerNameViewController=[[BpCustomerNameViewController alloc] initWithNibName:@"BpCustomerNameViewController" bundle:nil];
    
    gBpCustomerNameViewController.title=@"Name";
    //gBpAppointmentTypesViewController.selectedState=lblSelectedState.text;
    gBpCustomerNameViewController.gBpNewAppointmentViewController=self;
    [self.navigationController pushViewController:gBpCustomerNameViewController animated:YES];
    [gBpCustomerNameViewController release];
}

-(IBAction)ConfirmAppointmentTapped:(id)sender
{
    //userID=string&serviceReq=string&appDate=string&customerName=string&customerContact=string&customerEmail=string&appBranch=string&noOfPerson=string&custRemark=string&tokenID=string
    
    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    //NSLog(@"appDelegate.TokenString---->%@",appDelegate.TokenString);
    NSLog(@"appDelegate.loginStatus.ICNO---->%@",appDelegate.loginStatus.ICNO);
    
    
    NSString *usericNo=[[NSString alloc]initWithFormat:@"%@",appDelegate.loginStatus.ICNO];
    
    /**/if ([usericNo isEqualToString:@"(null)"]) {
        
        
        //NSString *usericNo=[[NSString alloc]initWithFormat:appDelegate.loginStatus.ICNO];
        //NSLog(@"usericNo2---->%@",usericNo);
        
        usericNo=@"";
    }
    
    //NSLog(@"usericNo----->%@",usericNo);
    //lblSelectedState
     if ([lblcustomerName.text length]!=0) {
         if ([lblcustomerName.text length]!=0) {
             
             if ([lblNoPerson.text length]!=0) {
                 if ([lblAppointmentBranch.text length]!=0) {
                     if ([lblemail.text length]!=0) {
                         if ([lblcontactno.text length]!=0) {
                             if ([lblAppointmentDate.text length]!=0) 
                             {
                                 
                                 if ([lblAppointmentType.text length] !=0) 
                                 {
                                     BpAppointment *Appitem=[[BpAppointment alloc]init];
                                     
                                     /**/Appitem.userID=usericNo;
                                     Appitem.appointmentServiceRequest=lblAppointmentType.text;
                                     Appitem.appointmentCreateDate=lblAppointmentDate.text;
                                     Appitem.customerName=lblcustomerName.text;
                                     Appitem.customerContact=lblcontactno.text;
                                     
                                     Appitem.customerEmail=lblemail.text;
                                     Appitem.appointmentBranch=lblAppointmentBranch.text;
                                     Appitem.appointmentNoOfPerson=lblNoPerson.text;
                                     Appitem.customerRemark=lblRemark.text;
                                     Appitem.tokenID=appDelegate.TokenString;
                                     
                                     NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
                                     BpMakeAppointmentRequest *loginset= [[BpMakeAppointmentRequest alloc] initWithsAppointment:Appitem];
                                     
                                     [networkHandler setDelegate:self];
                                     [networkHandler request:loginset];
                                     [loginset release];
                                     [networkHandler release];
                                     [Appitem release];
                                     [usericNo release];
                                     /**/UIView *selfView=self.view;
                                     LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
                                     self.loadingView=temploadingView;
                                     
                                 }else {
                                     /**/[self AlertInsertAll];
                                 }
                             }else {
                                 /**/[self AlertInsertAll];
                             }
                         }else {
                             /**/[self AlertInsertAll];
                         }
                     }else {
                         /**/[self AlertInsertAll];
                     }
                 }else {
                     /**/[self AlertInsertAll];
                 }
                 
             }else {
                 /**/[self AlertInsertAll];
             }
             
         }else {
             /**/[self AlertInsertAll];
         }

     }else {
         /**/[self AlertInsertAll];
     }
    
     
        
    

     
     
    //[self.navigationController popViewControllerAnimated:YES];
    //[self.gBpAppointmentViewController setNeedsDisplay];
}

-(void)AlertInsertAll
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"You are required to fill in all fields except the remark field. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
    [alertView release];
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpMakeAppointmentResponse class]])
    {
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpMakeAppointmentResponse*)responseMessage).AppointmentResponseArr;
        self.AppointmentArr=msgArr;
        
        for (BpAppointment *item in msgArr) {
            
            [self.loadingView removeView];
            //NSLog(@"userID--->%@",item.insertResult);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Appointment Submitted:" message:item.insertResult delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert autorelease];
            [alert show];
            
            [self.navigationController popToViewController: [self.navigationController.viewControllers objectAtIndex:0] animated:YES];
        } 
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    } 
    
}

- (void)dealloc 
{
    [gBpAppointmentViewController release];
    [loadingView release];
    [AppointmentArr release];
    [BpAppointmentitem release];
    [tblNewAppointment release];
    [lblSelectedState release];
    [lblAppointmentBranch release];
    [lblAppointmentType release];
    [lblAppointmentDate release];
    [lblRemark release];
    [lblcontactno release];
    [lblemail release];
    [lblNoPerson release];
    [scrollcontent release];
    [lblcustomerName release];
    [_btnconfirm release];
    [super dealloc];
}
@end
