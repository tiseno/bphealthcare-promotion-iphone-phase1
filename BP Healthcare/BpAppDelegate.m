//
//  BpAppDelegate.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpAppDelegate.h"

#import "BpViewController.h"

#import <AudioToolbox/AudioToolbox.h>

@implementation BpAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;
@synthesize EventArr, AppointmentArr;
@synthesize loginStatus, storedetail;
@synthesize TokenString;

- (void)dealloc
{
    [TokenString release];
    [storedetail release];
    [loginStatus release];
    [AppointmentArr release];
    [EventArr release];
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    self.viewController = [[[BpViewController alloc] initWithNibName:@"BpViewController" bundle:nil] autorelease];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
    
    NSDictionary *tmpDic = [launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
    
    //if tmpDic is not nil, then your app is launched due to an APNs push, therefore check this NSDictionary for further information
    if (tmpDic != nil) {
        BpAppPushViewController *BpHealthTips = [[BpAppPushViewController alloc] initWithNibName:@"BpAppPushViewController" bundle:[NSBundle    mainBundle]];
        self.window.rootViewController = BpHealthTips;
        [self.window makeKeyAndVisible];
        
    }
    
    
    // Let the device know we want to receive push notifications
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"hi~applicationDidEnterBackground~");
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"hi~applicationWillEnterForeground~");
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"hi~applicationDidBecomeActive~");
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	//NSLog(@"My token is: %@", deviceToken);
    //<3211f5a0 d9e94e20 03296bc2 743b0f96 d09361b8 a5f80857 9a9c65dd 80ece597>
    NSString *tokenstrg0=[[NSString alloc]initWithFormat:@"%@",deviceToken];
    NSString *tokenstr1=[tokenstrg0 stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSString *tokenstr2=[tokenstr1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *tokenstr3=[tokenstr2 stringByReplacingOccurrencesOfString:@">" withString:@""];
    NSLog(@"tokenstr3---> %@", tokenstr3);
    self.TokenString=tokenstr3;
    
    [tokenstrg0 release];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UIApplicationState state = [application applicationState];
     
    if (state == UIApplicationStateActive)
    {
    NSString *alertMsg;
    NSString *badge;
    NSString *sound;

    if( [[userInfo objectForKey:@"aps"] objectForKey:@"alert"] != NULL)
    {
        alertMsg = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"]; 
    }
    else
    {    alertMsg = @"{no alert message in dictionary}";
    }
    
    if( [[userInfo objectForKey:@"aps"] objectForKey:@"badge"] != NULL)
    {
        badge = [[userInfo objectForKey:@"aps"] objectForKey:@"badge"]; 
    }
    else
    {    badge = @"{no badge number in dictionary}";
    }
    
    if( [[userInfo objectForKey:@"aps"] objectForKey:@"sound"] != NULL)
    {
        sound = [[userInfo objectForKey:@"aps"] objectForKey:@"sound"]; 
    }
    else
    {    sound = @"{no sound in dictionary}";
    }
    

    AudioServicesPlayAlertSound (1007); 
    
    NSString* alert_msg = [NSString stringWithFormat:@"Message '%@' was just received.", alertMsg];
    
        alert = [[UIAlertView alloc] initWithTitle:@"BP Health Care"
                                           message:alert_msg
                                          delegate:self
                                 cancelButtonTitle:@"Cancel"
                                 otherButtonTitles:@"OK",nil];
    [alert show];
    [alert release];
    
    
    }
    else
    {
    
        BpAppPushViewController *BpHealthTips = [[BpAppPushViewController alloc] initWithNibName:@"BpAppPushViewController" bundle:[NSBundle    mainBundle]];
        self.window.rootViewController = BpHealthTips;
        [self.window makeKeyAndVisible];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0){
        [alert dismissWithClickedButtonIndex:0 animated:NO];
    }else{
        [alert dismissWithClickedButtonIndex:1 animated:NO];
        BpAppPushViewController *BpHealthTips = [[BpAppPushViewController alloc] initWithNibName:@"BpAppPushViewController" bundle:[NSBundle    mainBundle]];
        self.window.rootViewController = BpHealthTips;
        [self.window makeKeyAndVisible];
    }
}

-(void)clearloginStatus
{
    if (loginStatus!=nil) {
        [loginStatus release];
        loginStatus=nil;
    }
}

@end
