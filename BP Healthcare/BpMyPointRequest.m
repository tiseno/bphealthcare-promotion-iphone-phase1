//
//  BpMyPointRequest.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpMyPointRequest.h"

@implementation BpMyPointRequest
@synthesize userid;

-(id)initWithisicno:(NSString *)iicno
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"retrieveRewardRebatePoints";
        self.requestType=WebserviceRequest;
        self.userid=iicno;
    }
    return self;
}

-(void)dealloc
{
    [userid release];
    [super dealloc];
}

-(NSString*) generateHTTPPostMessage
{
    
    NSString *postMsg = [NSString stringWithFormat:@"userid=%@",self.userid];
    return postMsg;
} 


@end
