//
//  BpSelectBranchViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpSelectBranchViewController.h"

@interface BpSelectBranchViewController ()

@end

@implementation BpSelectBranchViewController
@synthesize gBpNewAppointmentViewController;
@synthesize tblBranchList, Branch, selectedState;
@synthesize loadingView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpSelectBranchViewController *gBpSelectBranchViewController=[[BpSelectBranchViewController alloc] initWithNibName:@"BpSelectBranchViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpSelectBranchViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpSelectBranchViewController release];
    
    
    tblBranchList.layer.cornerRadius=10;
    tblBranchList.layer.borderColor = [UIColor grayColor].CGColor;
    tblBranchList.layer.borderWidth = 1;
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
    [self getBranch];
}

- (void)viewDidUnload
{
    [self setTblBranchList:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)getBranch
{
    
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    BpOutletRequest *loginset= [[BpOutletRequest alloc] initWithsState:selectedState];
    
    [networkHandler setDelegate:self];
    [networkHandler request:loginset];
    [loginset release];
    [networkHandler release];
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpOutletResponse class]])
    {
        
        //NSString *strMessage=[NSString stringWithFormat:@"%@",((BpOutletResponse*)responseMessage).OutletArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpOutletResponse*)responseMessage).OutletArr;
        self.Branch=msgArr;
        
        //NSMutableArray *BizEntity= [[[NSMutableArray alloc]init] autorelease];
        
        /**/for (BpOutlet *item in msgArr) {
            //birthitem.resultCount;

            NSLog(@"BranchName--->%@",item.BranchName);
            
            if ([item.BranchName isEqualToString:@"none"]) 
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Please select a state. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertView show];
                [alertView release];
            }
            
        }
        
        //self.Branch=BizEntity;
        [tblBranchList reloadData];
        [self.loadingView removeView];
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    } 
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return Branch.count;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell.
    
    BpOutlet* classBpPromotions=[self.Branch objectAtIndex:indexPath.row];
    
    //NSString * sushiName = [Branch objectAtIndex:indexPath.row];
    //NSString *sushiString = [[NSString alloc] initWithFormat:@"%d: %@", indexPath.row, sushiName];
    NSString *sushiString = [[NSString alloc] initWithFormat:@" %@", classBpPromotions.BranchName];
    cell.textLabel.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
    cell.textLabel.text = sushiString;
    [sushiString release];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSString * sushiName = [Branch objectAtIndex:indexPath.row];
    BpOutlet* classBpPromotions=[self.Branch objectAtIndex:indexPath.row];
    NSString * sushiString = [NSString stringWithFormat:@"%@", classBpPromotions.BranchName];    
    NSString * message = [NSString stringWithFormat:@" %@", sushiString];
    /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Selected State" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
    [alertView release];*/
    
    
    UILabel *lblServiceTypeSelected=(UILabel*)[self.gBpNewAppointmentViewController lblAppointmentBranch];
    
    lblServiceTypeSelected.text=sushiString;
    
    [self.navigationController popViewControllerAnimated:YES];
    
    /*NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
     BpOutletRequest *loginset= [[BpOutletRequest alloc] initWithsState:sushiString];
     
     [networkHandler setDelegate:self];
     [networkHandler request:loginset];
     [loginset release];
     [networkHandler release];*/
    
    
    //_lastSushiSelected = sushiString;
    //SelectedState = [sushiString retain];
    
}

-(void)dealloc
{
    [loadingView release];
    [selectedState release];
    [Branch release];
    [gBpNewAppointmentViewController release];
    [tblBranchList release];
    [super dealloc];
}


@end


