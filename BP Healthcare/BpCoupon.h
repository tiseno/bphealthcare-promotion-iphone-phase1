//
//  BpCoupon.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BpCoupon : NSObject{
    
}

/*<resultCount>string</resultCount>
 <couponID>string</couponID>
 <couponTitle>string</couponTitle>
 <couponDescp>string</couponDescp>
 <couponSort>string</couponSort>
 <couponStatus>string</couponStatus>
 <couponDateTime>string</couponDateTime>
 <couponImgPath>string</couponImgPath>*/

@property (nonatomic,retain) NSString *resultCount;
@property (nonatomic,retain) NSString *couponID;
@property (nonatomic,retain) NSString *couponTitle;
@property (nonatomic,retain) NSString *couponDescp;
@property (nonatomic,retain) NSString *couponSort;
@property (nonatomic,retain) NSString *couponStatus;
@property (nonatomic,retain) NSString *couponDateTime;
@property (nonatomic,retain) NSString *couponImgPath;

@end
