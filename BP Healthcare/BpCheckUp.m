//
//  BpCheckUp.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpCheckUp.h"

@implementation BpCheckUp
@synthesize resultCount,checkupID,checkupTitle,checkupDescp,checkupSort,checkupStatus,checkupDateTime,checkupImgPath;

-(void)dealloc
{
    [checkupImgPath release];
    [checkupStatus release];
    [checkupDateTime release];
    [checkupSort release];
    [checkupDescp release];
    [checkupTitle release];
    [checkupID release];
    [resultCount release];
    [super dealloc];
}
@end
