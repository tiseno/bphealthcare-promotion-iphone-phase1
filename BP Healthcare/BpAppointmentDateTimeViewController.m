//
//  BpAppointmentDateTimeViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpAppointmentDateTimeViewController.h"

@interface BpAppointmentDateTimeViewController ()

@end

@implementation BpAppointmentDateTimeViewController
@synthesize datePicker;
@synthesize datelabel;
@synthesize gBpNewAppointmentViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BpAppointmentDateTimeViewController *gBpAppointmentDateTimeViewController=[[BpAppointmentDateTimeViewController alloc] initWithNibName:@"BpAppointmentDateTimeViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpAppointmentDateTimeViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];    
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(selectedDatetapped:) forControlEvents:UIControlEventTouchUpInside];   
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: rightButton] autorelease]; 
    
    gBpAppointmentDateTimeViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    [rightButton release];
    
    [leftButton release];
    [gBpAppointmentDateTimeViewController release];
    
    
    /*=======================*/
    //datePicker.datePickerMode =UIDatePickerModeDate;
    //[datePicker setDate:[NSDate date] animated:NO];
    [datePicker setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_add_appointment.png"]]];
    datePicker.datePickerMode =UIDatePickerModeDateAndTime;
    [datePicker setDate:[NSDate date] animated:NO];
    [datePicker addTarget:self action:@selector(LabelChange:) forControlEvents:UIControlEventValueChanged];
    
    
}

- (void)viewDidUnload
{
    [self setDatePicker:nil];
    [self setDatelabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)LabelChange:(id)sender
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    //[formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*2]];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:+3600*8]];
    [formatter setDateFormat:@"dd/MM/yyyy hh:mm a"];
    NSString* pickerDateStr = [formatter stringFromDate:[datePicker date]];
    NSDate* pickerDate = [formatter dateFromString:pickerDateStr];
    
    [formatter release];
    
	//NSDateFormatter *df = [[NSDateFormatter alloc] init];
   	//df.dateStyle = NSDateFormatterMediumStyle;
	//datelabel.text = [NSString stringWithFormat:@"%@", [df stringFromDate:datePicker.date]];
    datelabel.text = [NSString stringWithFormat:@"%@", pickerDateStr];
}

-(IBAction)selectedDatetapped:(id)sender
{
    UILabel *lblServiceTypeSelected=(UILabel*)[self.gBpNewAppointmentViewController lblAppointmentDate];
    
    lblServiceTypeSelected.text=datelabel.text;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc 
{
    [gBpNewAppointmentViewController release];
    [datePicker release];
    [datelabel release];
    [super dealloc];
}
@end
