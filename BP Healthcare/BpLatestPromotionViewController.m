//
//  BpLatestPromotionViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpLatestPromotionViewController.h"

@interface BpLatestPromotionViewController ()

@end

@implementation BpLatestPromotionViewController
@synthesize tblPromotion;
@synthesize promtArr, loadingView;
@synthesize enlargeImagePath;
@synthesize Bgimg, lblnoItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpLatestPromotionViewController *gBpLatestPromotionViewController=[[BpLatestPromotionViewController alloc] initWithNibName:@"BpLatestPromotionViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpLatestPromotionViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpLatestPromotionViewController release];
    
    [self getPromotions]; 
    lblnoItem.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
}

- (void)viewDidUnload
{
    [self setTblPromotion:nil];
    [self setBgimg:nil];
    [self setLblnoItem:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

/**/-(void)getPromotions
{
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    BpPromotionsRequest *requestset= [[BpPromotionsRequest alloc] init];
    
    [networkHandler setDelegate:self];
    [networkHandler request:requestset];
    [requestset release];
    [networkHandler release];
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpPromotionsResponse class]])
    {
        
        //NSString *strMessage=[NSString stringWithFormat:@"%@",((BpCheckUpResponse*)responseMessage).CheckUpArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpPromotionsResponse*)responseMessage).PromotionsArr;
        self.promtArr=msgArr;
        for (BpPromotions *item in msgArr)
         {
         
             /*
            //birthitem.resultCount;
            NSLog(@"birthitem.resultCount--->%@",item.resultCount);
            NSLog(@"birthitem.promotionID--->%@",item.promotionID);
            NSLog(@"birthitem.promotionDescp--->%@",item.promotionDescp);
            
            NSLog(@"birthitem.promotionSort--->%@",item.promotionSort);
            NSLog(@"birthitem.promotionStatus--->%@",item.promotionStatus);
            NSLog(@"birthitem.promotionTitle--->%@",item.promotionTitle);
            
            NSLog(@"birthitem.promotionDateTime--->%@",item.promotionDateTime);
            NSLog(@"birthitem.promotionImgPath--->%@",item.promotionImgPath);
            */
             
             if (![item.resultCount isEqualToString:@"0"])
             {
                 
                 Bgimg.hidden=YES;
                 lblnoItem.hidden=YES;
                 
             }else
             {
                 Bgimg.hidden=NO;
                 lblnoItem.hidden=NO;
             }

            
        }
        
        [tblPromotion reloadData];
        [self.loadingView removeView];
        
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    } 
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    cell = (BpPromotionCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.promtArr count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIDentifier=@"Cell";
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIDentifier];
    cell= (BpPromotionCell*)[tableView dequeueReusableCellWithIdentifier:cellIDentifier];
    if (cell == nil) {
        cell = [[[BpPromotionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIDentifier]autorelease];
    }else {
        
    }
    BpPromotions* classBpPromotions=[self.promtArr objectAtIndex:indexPath.row];
    
    cell.BpPromotionscell=classBpPromotions;
    //cell.lblTitle.text=classBpPromotions.promotionTitle;
    cell.imgsmallflag.image=[UIImage imageNamed:@"icon_promotions_2.png"];;
    
    //indeximg=indexPath.row;
    if (classBpPromotions.promotionImgPath.length!=0) 
    {
        
        
        /*=============*/
        
        CGSize maxsizetitle = CGSizeMake(220, 9999);
        UIFont *thefonttitle = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        CGSize textsizetitle = [classBpPromotions.promotionTitle sizeWithFont:thefonttitle constrainedToSize:maxsizetitle lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblTitle.numberOfLines=20;
        cell.lblTitle.frame=CGRectMake(80, 10, 220, textsizetitle.height);
        cell.lblTitle.lineBreakMode=UILineBreakModeWordWrap;
        cell.lblTitle.text=classBpPromotions.promotionTitle;
        
        
        
        CGSize maxsize = CGSizeMake(220, 9999);
        UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:12];
        CGSize textsize = [classBpPromotions.promotionDescp sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblBodytext.numberOfLines=20;
        cell.lblBodytext.frame=CGRectMake(80, textsizetitle.height+15, 220, textsize.height);
        cell.lblBodytext.lineBreakMode=UILineBreakModeWordWrap;
        cell.lblBodytext.text=classBpPromotions.promotionDescp;
        
        
        if ((textsizetitle.height+textsize.height+30)<130) {
            
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, 150);
        }else
        {
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsizetitle.height+textsize.height+30);
        }
        
        /*=============*/
        
         
        NSString *itemimg=[classBpPromotions.promotionImgPath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/promotion/%@",itemimg];
        cell.url=imagename;
        cell.imgpicture.imageURL = [NSURL URLWithString:imagename];
 
        
        
    }else 
    {
        /*=============*/
        CGSize maxsize = CGSizeMake(220, 9999);
        UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:12];
        CGSize textsize = [classBpPromotions.promotionDescp sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblBodyNoPictext.numberOfLines=20;
        cell.lblBodyNoPictext.frame=CGRectMake(80, 35, 220, textsize.height+50);
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsize.height+100);
        
        /*=============*/
        
        cell.lblBodyNoPictext.text=classBpPromotions.promotionDescp;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //[cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    
    return cell;
}

-(void)ImageTapped
{
    NSLog(@"hello!!!");
    //NSLog(@"self.enlargeImagePath!!!--->%@",self.enlargeImagePath);
    //NSString *largeimage = [imagepath stringByReplacingCharactersInRange:NSMakeRange(imagepath.length - 5, 1) withString:@"M"];
    //[MTPopupWindow showWindowWithHTMLFile:largeimage insideView:self.view];
    
    NSLog(@"indeximg--->%d", indeximg);
    BpPromotions* classBpPromotions=[self.promtArr objectAtIndex:indeximg];
    NSLog(@"classBpPromotions--->%@", classBpPromotions.promotionImgPath);
    
    
    
    /*for (BpPromotions *imgitm in classBpPromotions) {
        
        //imgitm.promotionImgPath;
        //NSLog(@"imgitm.promotionImgPath--->%@", imgitm.promotionImgPath);
    }
    
    for(int i=0;i<classBpPromotions.count;i++)
    {
        
    }*/
    
    //[MTPopupWindow showWindowWithHTMLFile:self.enlargeImagePath insideView:self.view];
    [enlargeImagePath release];
}

-(void)showloadingView
{
    [self.loadingView removeView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BpPromotionCell *selectedcell= (BpPromotionCell*)[tableView cellForRowAtIndexPath:indexPath]; 
    //NSLog(@"selectedcell.BpPromotionscell.promotionImgPath!!--->%@",selectedcell.BpPromotionscell.promotionImgPath);
    
    //BpPromotions* classBpPromotions=[self.promtArr objectAtIndex:indexPath.row];
    
    //cell.BpPromotionscell=classBpPromotions;
    
    if (![selectedcell.BpPromotionscell.promotionImgPath isEqualToString:@"none"])
    {
    
    NSString *itemimg=[selectedcell.BpPromotionscell.promotionImgPath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/promotion/a%@",itemimg];
    
    BpPromotionImageViewController *gBpPromotionImageViewController=[[BpPromotionImageViewController alloc] initWithNibName:@"BpPromotionImageViewController" bundle:nil];
    gBpPromotionImageViewController.title=@"Promotions";
    gBpPromotionImageViewController.imgPath=imagename;
    [self.navigationController pushViewController:gBpPromotionImageViewController animated:YES];
    [gBpPromotionImageViewController release];
    }
    
    /*[MTPopupWindow showWindowWithHTMLFile:imagename insideView:self.view];
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
    [self performSelector:@selector(showloadingView) withObject:nil afterDelay:2];
    */
    
    /*if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    { 
        
        
        
        BpPromotionCell *selectedcell= (BpPromotionCell*)[tableView cellForRowAtIndexPath:indexPath];    
        //NSLog(@"%d",selectedcell.CAgendaEvent.AgendaEventID );
        
        
        AgendaEventDetailView *gAgendaEventDetailView=[[AgendaEventDetailView alloc] initWithNibName:@"AgendaEventDetailView" bundle:nil];
        
        NSString *sellectedAgendaID=[[NSString alloc]initWithFormat:@"%d",selectedcell.CAgendaEvent.AgendaEventID];
        gAgendaEventDetailView.gAgendaDetainID=sellectedAgendaID;
        gAgendaEventDetailView.agendaEvent=selectedcell.CAgendaEvent;
        gAgendaEventDetailView.title=@"Tupperware Brands Asia Pasific";
        
        [self.navigationController pushViewController:gAgendaEventDetailView animated:YES];*/
        
        /*BulletinBoardDetail *gBulletinBoardDetail=[[BulletinBoardDetail alloc] initWithNibName:@"BulletinBoardDetail" bundle:nil];
         gBulletinBoardDetail.BulletinID= selectedcell.bulletinMessage.ID;
         gBulletinBoardDetail.Bulletintitle=selectedcell.bulletinMessage.Title;
         gBulletinBoardDetail.Bulletintimedate=selectedcell.bulletinMessage.Time_Date;
         gBulletinBoardDetail.Bulletinbody=selectedcell.bulletinMessage.Body;
         gBulletinBoardDetail.Bulletinimagepath=selectedcell.bulletinMessage.ImagePath;
         
         
         gBulletinBoardDetail.title=@"Tupperware Brands Asia Pasific";
         
         [self.navigationController pushViewController:gBulletinBoardDetail animated:YES];*/
    //}
}



- (void)dealloc 
{
    [enlargeImagePath release];
    [loadingView release];
    [promtArr release];
    [tblPromotion release];
    [Bgimg release];
    [lblnoItem release];
    [super dealloc];
}


@end
